package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.models.sliderImages.SliderImagesDataModel;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

public class ImageSliderAdapter extends RecyclerView.Adapter<ImageSliderAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SliderImagesDataModel> list;
    ViewPager2 viewPager2;

    public ImageSliderAdapter(Context context, ArrayList<SliderImagesDataModel> list, ViewPager2 viewPager2) {
        this.context = context;
        this.list = list;
        this.viewPager2 = viewPager2;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.design_image_slider_container, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SliderImagesDataModel model = list.get(position);
        String imageurl = model.getSlider_image();
        if (!TextUtils.isEmpty(imageurl)) {
            holder.imageLoading.setVisibility(View.VISIBLE);
            Glide.with(context).load(RetrofitClientInstance.BASE_URL_IMG + imageurl).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.imageLoading.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    holder.imageLoading.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.image);
//                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
        }

        if (position == list.size() - 2) {

            viewPager2.post(runnable);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView image;
        AVLoadingIndicatorView imageLoading;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.slider_image);
            imageLoading = itemView.findViewById(R.id.imageLoading);
        }

        void setImage(SliderImagesDataModel imageSliderModel) {
            Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + imageSliderModel.getSlider_image()).into(image);
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            list.addAll(list);
            notifyDataSetChanged();
        }
    };
}


