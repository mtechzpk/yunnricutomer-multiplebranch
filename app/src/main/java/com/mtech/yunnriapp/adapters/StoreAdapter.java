package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.Home1Activity;
import com.mtech.yunnriapp.models.StoreModel;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.List;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.ViewHolder> {

    private Context context;


    private List<StoreModel> list;

    public StoreAdapter(Context context, List<StoreModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_freshfood_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        StoreModel model1 = list.get(position);
        holder.categoryName.setText(model1.getSubcategory_name());
        Glide.with(context).load(RetrofitClientInstance.BASE_URL_IMG + model1.getProfile_image()).into(holder.imageView_store);
        holder.storeName.setText(model1.getShop_name());
//        holder.deliveryCharges.setText(model1.getDelivery_area());
        holder.deliveryCharges.setText(model1.getDelivery_charges()+model1.getDelivery_charges_currency());
//        holder.deliveryTime.setText(model1.getTotal_revenue());
//        holder.deliveryTime.setText("30m");
        holder.minPurchase.setText("Min." + model1.getMin_purchase() + "" + model1.getMin_purchase_currency());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String subImage = model1.getSubcategory_image();
                String subName = model1.getSubcategory_name();
                int subId = model1.getSubcategory_id();
                int storeId = model1.getId();
                String storeImage = model1.getProfile_image();
                String minPurchase = model1.getMin_purchase();
                String storeName = model1.getFirst_name() + " " + model1.getLast_name();

                Intent intent = new Intent(context, Home1Activity.class);
                Utilities.saveString(context, "subImage", subImage);
                Utilities.saveString(context, "subName", subName);
                Utilities.saveInt(context, "storeId", storeId);
                Utilities.saveInt(context, "subId", subId);
                Utilities.saveString(context, "minPurchase", minPurchase);
                Utilities.saveString(context, "storeImage", storeImage);
                Utilities.saveString(context, "storeName", model1.getShop_name());
                Utilities.saveString(context, "storeMinPrice", model1.getMin_purchase());
                Utilities.saveString(context, "storeCurrency", model1.getMin_purchase_currency());

                Utilities.saveString(context, "store_lat", model1.getLatitude());
                Utilities.saveString(context, "store_long", model1.getLongitude());


                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        SelectableRoundedImageView imageView_store;
        TextView storeName, categoryName, deliveryTime, deliveryCharges, minPurchase;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView_store = itemView.findViewById(R.id.storeImage);
            storeName = itemView.findViewById(R.id.store_name);
            categoryName = itemView.findViewById(R.id.subCategoryName);
//            deliveryTime = itemView.findViewById(R.id.deliveryTime);
            deliveryCharges = itemView.findViewById(R.id.deliverycharges);
            minPurchase = itemView.findViewById(R.id.minPurchase);
            storeName.setSelected(true);
            categoryName.setSelected(true);
        }

    }
}




