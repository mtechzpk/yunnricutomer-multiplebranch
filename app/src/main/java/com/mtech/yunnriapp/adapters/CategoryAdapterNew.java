package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.Home1Activity;
import com.mtech.yunnriapp.models.CategoryDataModel;
import com.mtech.yunnriapp.models.CategoryModel;
import com.mtech.yunnriapp.models.CategoryModelNew;
import com.mtech.yunnriapp.models.SubCategoryModel;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CategoryAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CategoryModelNew> categoryModels;

    public CategoryAdapterNew(Context context, ArrayList<CategoryModelNew> categoryModels) {
        this.context = context;
        this.categoryModels = categoryModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_freshfood_layout, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

        Glide.with(context)
                .load(RetrofitClientInstance.BASE_URL_IMG+categoryModels.get(position).getProfile_image()).centerCrop().into(((BookViewHolder) holder).imageView_store);
        holder1.storeName.setText(categoryModels.get(position).getName());
      //  holder1.categoryName.setText(categoryModels.get(position).getSubcategory_name());
        holder1.deliveryTime.setText(categoryModels.get(position).getDelivery_time());
        holder1.deliveryCharges.setText(categoryModels.get(position).getDelivery_charges());
        holder1.minPurchase.setText(categoryModels.get(position).getMin_purchase());

        holder1.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //    String subcategoryName = categoryModels.get(position).getSubcategory_name();
                String storeName = categoryModels.get(position).getName();
                String storeImage = categoryModels.get(position).getProfile_image();
            //      String subcategoryImage = categoryModels.get(position).getSubcategory_image();
               Intent intent = new Intent(context, Home1Activity.class);
//
            //    Utilities.saveString(context,"categoryName",subcategoryName);
                Utilities.saveString(context,"storeName",storeName);
                Utilities.saveString(context,"storeImage",storeImage);
             //   Utilities.saveString(context,"subcategoryImage",subcategoryImage);

               context.startActivity(intent);

                Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {
        SelectableRoundedImageView imageView_store;
        TextView storeName,categoryName,deliveryTime,deliveryCharges,minPurchase;
        LinearLayout layout;

        private BookViewHolder(View itemView) {
            super(itemView);
            imageView_store = itemView.findViewById(R.id.storeImage);
            storeName = itemView.findViewById(R.id.store_name);
            categoryName = itemView.findViewById(R.id.subCategoryName);
            deliveryTime = itemView.findViewById(R.id.deliveryTime);
            deliveryCharges = itemView.findViewById(R.id.deliverycharges);
            minPurchase = itemView.findViewById(R.id.minPurchase);
            layout = itemView.findViewById(R.id.layout_item);

        }

        private void bind(int pos) {
            CategoryModelNew messagesTabModel = categoryModels.get(pos);
            storeName.setText(messagesTabModel.getName());
//            initClickListener();
        }

//        private void initClickListener() {
//            rlItems.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
//        }
    }

    public interface Callback {
        void onItemClick(int pos);
    }

}
