package com.mtech.yunnriapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.FavStoreModel;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.activities.Home1Activity;
import com.mtech.yunnriapp.activities.HomeActivity;
import com.mtech.yunnriapp.models.FavouriteStoresModel;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import okhttp3.internal.Util;

public class FavouriteStoresAdapter extends RecyclerView.Adapter<FavouriteStoresAdapter.ViewHolder> {

    private Context context;
    private List<FavStoreModel> list;

    public FavouriteStoresAdapter(Context context, List<FavStoreModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.single_favourite_stores_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        FavStoreModel model = list.get(position);
        Glide.with(context).load(RetrofitClientInstance.BASE_URL_IMG + model.getImage()).into(holder.iv_image);
        holder.tv_StoreName.setText(model.getName());
        holder.tv_Title.setText(model.getCatName());
        holder.tv_MinPrice.setText("Min" + model.getMinPurchase() + model.getCurrency());
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();
                MyDatabase database = Room.databaseBuilder(context, MyDatabase.class, "MyFav").allowMainThreadQueries().build();
                int id = list.get(position).getStoreId();
                database.cartDao().deleteFavItem(id);
                list.remove(position);
                notifyDataSetChanged();

            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Home1Activity.class);
                String isFav = model.getIsFav();
                int storeId = model.getStoreId();
                Utilities.saveString(context, "isFav", "true");
                Utilities.saveInt(context, "storeId", storeId);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private SelectableRoundedImageView iv_image;
        private TextView tv_StoreName, tv_Title, tv_Time, tv_price, tv_MinPrice;
        private LinearLayout layout;
        ImageView ivDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_image = itemView.findViewById(R.id.iv_img);
            tv_StoreName = itemView.findViewById(R.id.tv_store_name);
            tv_Title = itemView.findViewById(R.id.tv_title);
            tv_Time = itemView.findViewById(R.id.tv_time);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_MinPrice = itemView.findViewById(R.id.tv_min_price);
            layout = itemView.findViewById(R.id.layout);
            ivDelete = itemView.findViewById(R.id.ivDelete);


        }
    }
}
