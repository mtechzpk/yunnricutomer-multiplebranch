package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.mtech.yunnriapp.CartClickListner;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.Cart;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    List<Cart> carts;
    Context context;
    private MyDatabase myAppDatabase;
    int number;
    private CartClickListner cartClickListner;



    public CartAdapter(List<Cart> carts, Context context,CartClickListner listener) {
        this.carts = carts;
        this.context = context;
        this.cartClickListner = listener;
        myAppDatabase = Room.databaseBuilder(context,MyDatabase.class,"My_Cart").allowMainThreadQueries().build();



    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cart_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Cart cart = carts.get(position);
        String prodcut_currency = Utilities.getString(context, "prodcut_currency");

        float price = Float.parseFloat(cart.getPrice());
        float qty = Float.parseFloat(cart.getQuantity());

        float product = price * qty;
        holder.tv_Price.setText(prodcut_currency + String.valueOf(product));
        holder.tv_Name.setText(cart.getName());
        holder.tv_quantity.setText(cart.getQuantity());
        holder.ivCross.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();
                MyDatabase database = Room.databaseBuilder(context, MyDatabase.class, "My_Cart").allowMainThreadQueries().build();
                int id = carts.get(position).getId();
                database.cartDao().deleteItem(id);
                carts.remove(position);
                notifyDataSetChanged();
                cartClickListner.onDeleteClick(cart);


            }
        });

        int count = carts.size();

        Utilities.saveInt(context, "TotalCartItem", count);


        holder.btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                number = Integer.parseInt(holder.tv_quantity.getText().toString());
                if (number <= 1) {
                    number = 1;
                } else {

                    number--;
                    holder.tv_quantity.setText("" + number);
                }

                String price = cart.getPrice();
                double value1 = price.isEmpty() ? 0.0 : Double.parseDouble(price);

                String quantity = holder.tv_quantity.getText().toString();
                double value2 = quantity.isEmpty() ? 0.0 : Double.parseDouble(quantity);
                double a;
                a = value1 * value2;
                String finalValue = Double.toString(a);
                holder.tv_Price.setText(finalValue);
                myAppDatabase.cartDao().update(String.valueOf(quantity),carts.get(position).getId());
                cartClickListner.onMinusClick(cart);



            }

        });

        holder.btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                number = Integer.parseInt(holder.tv_quantity.getText().toString());
                number++;

                holder.tv_quantity.setText("" + number);

                String price = cart.getPrice();
                double value1 = price.isEmpty() ? 0.0 : Double.parseDouble(price);

                String quantity = holder.tv_quantity.getText().toString();
                double value2 = quantity.isEmpty() ? 0.0 : Double.parseDouble(quantity);


                double a;
                a = value1 * value2;

                String finalValue = Double.toString(a);
                holder.tv_Price.setText(finalValue);
                myAppDatabase.cartDao().update(String.valueOf(quantity),carts.get(position).getId());
                cartClickListner.onPlusClick(cart);



            }
        });

    }


    @Override
    public int getItemCount() {
        return carts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_quantity, tv_Name, tv_Price;
        private ImageView btn_add;
        private LinearLayout btn_minus;
        ImageView ivCross;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_quantity = itemView.findViewById(R.id.tv_no);
            ivCross = itemView.findViewById(R.id.ivCross);
            btn_add = itemView.findViewById(R.id.btn_add);
            btn_minus = itemView.findViewById(R.id.btn_minus);
            tv_Name = itemView.findViewById(R.id.tv_name);
            tv_Price = itemView.findViewById(R.id.tv_price);

        }
    }

    private void showCustomDialogforDelete() {
        final PrettyDialog pDialog = new PrettyDialog(context);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Delete This Item?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimaryDark)
                .addButton(
                        "Yes",
                        R.color.colorPrimary,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_red,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();
    }
}

