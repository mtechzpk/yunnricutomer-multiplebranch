package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.Cart;
import com.mtech.yunnriapp.models.OrderDetailsModel;

import java.util.ArrayList;
import java.util.List;

public class OrdersDetailsAdapter extends RecyclerView.Adapter<OrdersDetailsAdapter.ViewHolder> {

    private Context context;
    private List<Cart> list;

    public OrdersDetailsAdapter(Context context,List<Cart> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_orders_details_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Cart model = list.get(position);
        Glide.with(context).load(model.getImage()).into(holder.iv_image);
        holder.tv_Weight.setText(model.getQuantity());
        holder.tv_Title.setText(model.getName());
        holder.tv_price.setText(model.getPrice());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView iv_image;
        private TextView tv_Weight, tv_Title, tv_price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_image = itemView.findViewById(R.id.iv_img);
            tv_Weight = itemView.findViewById(R.id.tv_weight);
            tv_Title = itemView.findViewById(R.id.tv_title);
            tv_price = itemView.findViewById(R.id.tv_price);
        }
    }
}
