package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.AddressesActivity;
import com.mtech.yunnriapp.activities.EditAddressActivity;
import com.mtech.yunnriapp.activities.HomeActivity;
import com.mtech.yunnriapp.activities.MainActivity;
import com.mtech.yunnriapp.models.AddressResponseModel;
import com.mtech.yunnriapp.models.AddressesModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressesAdapter extends RecyclerView.Adapter<AddressesAdapter.ViewHolder> {
    private Context context;
    private List<AddressesModel> list;
    String addressID;

    KProgressHUD hud;


    public AddressesAdapter(Context context, List<AddressesModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.single_address_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AddressesModel model = list.get(position);

        holder.tv_addressType.setText(model.getAddress_type());
        holder.tv_plot.setText(model.getFloor());
        holder.tv_City.setText(model.getStreet());

        addressID = String.valueOf(model.getId());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String deliverTo = model.getAddress_type();
                Intent intent = new Intent(context, HomeActivity.class);
                Utilities.saveString(context,"deliverTo",deliverTo);
                Utilities.saveString(context,"floor",model.getFloor());
                Utilities.saveString(context,"city",model.getStreet());
                context.startActivity(intent);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDialog();
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String addressId = String.valueOf(model.getId());
                Intent intent = new Intent(context, EditAddressActivity.class);
                Utilities.saveString(context,"addressId",addressId);
                context.startActivity(intent);
            }
        });
    }

    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(context);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to delete this Address?")
                .setIcon(R.drawable.ic_baseline_delete_forever_24)
                .setIconTint(R.color.color_Topbar_yellow)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.color_Topbar_yellow,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                deleteProductApi();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.black,
                        R.color.grey,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void deleteProductApi() {
        hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<AddressResponseModel> call = service.deleteAddress(addressID);
        call.enqueue(new Callback<AddressResponseModel>() {
            @Override
            public void onResponse(Call<AddressResponseModel> call, Response<AddressResponseModel> response) {

                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200)
                {
                    hud.dismiss();
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, AddressesActivity.class);
                    context.startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<AddressResponseModel> call, Throwable t) {

                hud.dismiss();
                t.printStackTrace();
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_addressType, tv_plot, tv_City;
        ImageView delete, edit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_addressType = itemView.findViewById(R.id.tv_addressType);
            tv_plot = itemView.findViewById(R.id.tv_plot);
            tv_City = itemView.findViewById(R.id.tv_city);
            edit = itemView.findViewById(R.id.btn_edit);
            delete = itemView.findViewById(R.id.btn_delete);


        }
    }
}