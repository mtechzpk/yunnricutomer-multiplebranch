package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.models.NotificationModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    Context context;
    List<NotificationModel> notificationModels;

    public NotificationAdapter(Context context, List<NotificationModel> notificationModels) {
        this.context = context;
        this.notificationModels = notificationModels;
    }
    @NonNull
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_notification_item, parent, false);
        return new NotificationAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.ViewHolder holder, int position) {
        NotificationModel model = notificationModels.get(position);

        DateFormat theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null, date1 = null;

        try {
            date = theDateFormat.parse(model.getCreated_at());
        } catch (ParseException parseException) {
            // Date is invalid. Do what you want.
        } catch (Exception exception) {
            // Generic catch. Do what you want.
        }

        theDateFormat = new SimpleDateFormat("MMM dd, yyyy");
        String[] arrayString = model.getCreated_at().split(";");

        String time = arrayString[0];

        time = time.substring(time.indexOf("2020-10-12T") + 12, time.length());
//        tvDate.setText(theDateFormat.format(date));
        DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = null;
        try {
            d = f1.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("h:mm a");
        String timeFormate = f2.format(d).toLowerCase(); // "12:18am"
        holder.tvDate.setText(theDateFormat.format(date));

        holder.notificationText.setText(model.getNotification());
        holder.tvNotificationFrom.setText(model.getNotification_from());
    }

    @Override
    public int getItemCount() {
        return notificationModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate,tvNotificationFrom,sku,price,notificationText;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            tvDate = itemView.findViewById(R.id.tvDate);
            tvNotificationFrom = itemView.findViewById(R.id.tvNotificationFrom);
            notificationText = itemView.findViewById(R.id.notificationText);
            tvDate = itemView.findViewById(R.id.tvDate);
        }
    }
}
