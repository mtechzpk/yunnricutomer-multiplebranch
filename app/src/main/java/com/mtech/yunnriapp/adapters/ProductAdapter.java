package com.mtech.yunnriapp.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.models.ProductDataModel;

import java.util.List;

public class ProductAdapter
        extends RecyclerView
        .Adapter<ProductAdapter.ParentViewHolder> {

    Context context;
    // An object of RecyclerView.RecycledViewPool
    // is created to share the Views
    // between the child and
    // the parent RecyclerViews
    private RecyclerView.RecycledViewPool
            viewPool
            = new RecyclerView
            .RecycledViewPool();
    private List<ProductDataModel> itemList;

    public ProductAdapter(List<ProductDataModel> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public ParentViewHolder onCreateViewHolder(
            @NonNull ViewGroup viewGroup,
            int i) {

        // Here we inflate the corresponding
        // layout of the parent item
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(
                        R.layout.product_layout,
                        viewGroup, false);

        return new ParentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(
            @NonNull ParentViewHolder parentViewHolder,
            int position) {

        // Create an instance of the ParentItem
        // class for the given position
        ProductDataModel parentItem
                = itemList.get(position);

        parentViewHolder.Product_Category.setText(parentItem.getProduct_category());


        // Here we have assigned the layout
        // as LinearLayout with vertical orientation
        GridLayoutManager layoutManager
                = new GridLayoutManager(parentViewHolder.ChildRecyclerView.getContext(), 2, RecyclerView.VERTICAL, false);
//                parentViewHolder
//                        .ChildRecyclerView
//                        .getContext(),
//                LinearLayoutManager.VERTICAL,
//                false);

        // Since this is a nested layout, so
        // to define how many child items
        // should be prefetched when the
        // child RecyclerView is nested
        // inside the parent RecyclerView,
        // we use the following method
        layoutManager
                .setInitialPrefetchItemCount(
                        parentItem
                                .getProducts()
                                .size());

        // Create an instance of the child
        // item view adapter and set its
        // adapter, layout manager and RecyclerViewPool
        ProductlistAdapter childItemAdapter
                = new ProductlistAdapter(
                parentItem
                        .getProducts(), context);
        parentViewHolder
                .ChildRecyclerView
                .setLayoutManager(layoutManager);
        parentViewHolder
                .ChildRecyclerView
                .setAdapter(childItemAdapter);
        parentViewHolder
                .ChildRecyclerView
                .setRecycledViewPool(viewPool);
    }

    // This method returns the number
    // of items we have added in the
    // ParentItemList i.e. the number
    // of instances we have created
    // of the ParentItemList
    @Override
    public int getItemCount() {

        return itemList.size();
    }

    // This class is to initialize
    // the Views present in
    // the parent RecyclerView
    class ParentViewHolder
            extends RecyclerView.ViewHolder {

        private TextView Product_Category;
        private RecyclerView ChildRecyclerView;

        ParentViewHolder(final View itemView) {
            super(itemView);

            Product_Category
                    = itemView
                    .findViewById(
                            R.id.productName);
            ChildRecyclerView
                    = itemView
                    .findViewById(
                            R.id.product_item_recyclerview);
        }
    }
}
