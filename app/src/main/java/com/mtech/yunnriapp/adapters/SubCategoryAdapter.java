package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.models.SubCategoriesIDModel;

import java.util.ArrayList;

public class SubCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private Callback callback;
    private ArrayList<SubCategoriesIDModel> subCategoriesIDModels;
    KProgressHUD hud;
    int product_id;
    public static int selected_position = -1;

    public SubCategoryAdapter(Context context, ArrayList<SubCategoriesIDModel> subCategoriesIDModels, Callback callback) {
        this.context = context;
        this.callback = callback;
        this.subCategoriesIDModels = subCategoriesIDModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_subcategory, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;


        holder1.tvproductName.setText(subCategoriesIDModels.get(position).getSubCategoryName());
//
        holder1.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callback.onItemClick(position, holder1.tvproductName);

            }
        });

    }

    public interface Callback {
        void onItemClick(int pos, TextView tvproductName);
    }

    @Override
    public int getItemCount() {
        return subCategoriesIDModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvproductName;
        LinearLayout rlItems;


        private BookViewHolder(View itemView) {
            super(itemView);

//init views
            tvproductName = itemView.findViewById(R.id.tvName);
            rlItems = itemView.findViewById(R.id.rlItems);
        }

//        private void bind(int pos,TextView textView) {
//            SubCategoriesIDModel messagesTabModel = subCategoriesIDModels.get(pos);
//            subCategoriesIDModels.get(pos).setSelected(true);
//            for (int i = 0; i <subCategoriesIDModels.size() ; i++) {
//                if (i!=pos)
//                {
//                    subCategoriesIDModels.get(pos).setSelected(false);
//                }
//
//            }
//
//            initClickListener();
//        }

        private void initClickListener() {
            rlItems.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(getAdapterPosition(), tvproductName);
                    notifyDataSetChanged();
                }
            });
        }
    }


}