package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.Home1Activity;
import com.mtech.yunnriapp.models.FreshFoodModel;
import com.joooonho.SelectableRoundedImageView;

import java.util.ArrayList;

public class FreshFoodAdapter extends RecyclerView.Adapter<FreshFoodAdapter.ViewHolder> {

    private Context context;
    private ArrayList<FreshFoodModel> list;

    public FreshFoodAdapter(Context context, ArrayList<FreshFoodModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_freshfood_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        FreshFoodModel model = list.get(position);

        holder.iv_image.setImageResource(R.drawable.food_image);
        holder.tv_StoreName.setText(model.getStoreName());
        holder.tv_Title.setText(model.getTitle());
        holder.tv_Time.setText(model.getTime());
        holder.tv_price.setText(model.getPrice());
        holder.tv_MinPrice.setText(model.getMinPrice());

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(context, Home1Activity.class);
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private SelectableRoundedImageView iv_image;
        private TextView tv_StoreName, tv_Title, tv_Time, tv_price, tv_MinPrice;
        private LinearLayout layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_image = itemView.findViewById(R.id.iv_img);
            tv_StoreName = itemView.findViewById(R.id.tv_store_name);
            tv_Title = itemView.findViewById(R.id.tv_title);
            tv_Time = itemView.findViewById(R.id.tv_time);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_MinPrice = itemView.findViewById(R.id.tv_min_price);
            layout = itemView.findViewById(R.id.layout);


        }
    }
}
