package com.mtech.yunnriapp.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.AddCartActivity;
import com.mtech.yunnriapp.models.ProductlistModel;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.List;

public class ProductlistAdapter extends RecyclerView.Adapter<ProductlistAdapter.ChildViewHolder> {

    List<ProductlistModel> ChildItemList;
    Context context;

    // Constuctor
    ProductlistAdapter(List<ProductlistModel> childItemList,Context context)
    {
        this.ChildItemList = childItemList;
        this.context =context;
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateViewHolder(
            @NonNull ViewGroup viewGroup,
            int i)
    {

        // Here we inflate the corresponding
        // layout of the child item
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(
                        R.layout.single_vegetable_layout,
                        viewGroup, false);

        return new ChildViewHolder(view);
    }

    @Override
    public void onBindViewHolder(
            @NonNull ChildViewHolder childViewHolder,
            int position)
    {

        ProductlistModel model = ChildItemList.get(position);

        childViewHolder.name.setText(model.getProduct_name());
        childViewHolder.weight.setText(model.getPrice_per_unit_weight()+model.getUnit_weight());
        childViewHolder.price.setText(model.getPrice()+" "+model.getPrice_currency()+"/"+model.getPrice_per_unit_weight()+model.getUnit_weight());

        Glide.with(context).load(RetrofitClientInstance.BASE_URL_IMG+model.getImage()).into(childViewHolder.imageView_product);

        childViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productImage = model.getImage();
                String productName = model.getProduct_name();
                String productWeight = model.getUnit_weight();
                String productPrice = model.getPrice();
                int productId = model.getId();

                Intent intent = new Intent(context, AddCartActivity.class);
                Utilities.saveString(context,"productImage",productImage);
                Utilities.saveString(context,"productName",productName);
                Utilities.saveString(context,"productWeight",productWeight);
                Utilities.saveString(context,"productPrice",productPrice);
                Utilities.saveString(context,"prodcut_description",model.getProduct_description());
                Utilities.saveString(context,"prodcut_currency",model.getPrice_currency());
                Utilities.saveInt(context,"productId",productId);

                context.startActivity(intent);
            }
        });

        // Create an instance of the ChildItem
        // class for the given position

    }

    @Override
    public int getItemCount()
    {

        // This method returns the number
        // of items we have added
        // in the ChildItemList
        // i.e. the number of instances
        // of the ChildItemList
        // that have been created
        return ChildItemList.size();
    }

    // This class is to initialize
    // the Views present
    // in the child RecyclerView
    class ChildViewHolder
            extends RecyclerView.ViewHolder {

        TextView name,weight,price;
        RelativeLayout add;
        ImageView imageView_product;


        ChildViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.tv_name_product);
            weight = itemView.findViewById(R.id.tv_weight_product);
            price = itemView.findViewById(R.id.tv_price_product);
            imageView_product = itemView.findViewById(R.id.iv_img_product);
            add = itemView.findViewById(R.id.btn_add_product);
        }
    }
}
