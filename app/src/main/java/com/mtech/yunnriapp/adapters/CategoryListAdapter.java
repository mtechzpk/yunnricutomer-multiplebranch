package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.HomeActivity;
import com.mtech.yunnriapp.models.CategoryListModel;
import com.mtech.yunnriapp.models.StoreModel;
import com.mtech.yunnriapp.models.SubCategoryModel;
import com.mtech.yunnriapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    private Context context;
    private List<SubCategoryModel> list;



    public CategoryListAdapter(Context context, List<SubCategoryModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_list_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        SubCategoryModel model = list.get(position);
        holder.type.setText(model.getSubcategory_name());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public  class  ViewHolder extends RecyclerView.ViewHolder{

        private TextView type;
        private LinearLayout inner_Layout;
        private RecyclerView recyclerView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            type = itemView.findViewById(R.id.tv_type);
            inner_Layout= itemView.findViewById(R.id.inner_layout);
        }
    }
}
