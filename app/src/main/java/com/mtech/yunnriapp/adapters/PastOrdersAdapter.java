package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.AddMoreActivity;
import com.mtech.yunnriapp.models.OrderDataModel;
import com.mtech.yunnriapp.models.PastOrdersModel;
import com.mtech.yunnriapp.utils.Utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PastOrdersAdapter extends RecyclerView.Adapter<PastOrdersAdapter.ViewHolder> {

    private Context context;
    private List<OrderDataModel> list;

    public PastOrdersAdapter(Context context, List<OrderDataModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_past_orders_layout,parent,false);
        return  new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OrderDataModel model = list.get(position);

        String storename = model.getStore_details().getFirst_name() + " " + model.getStore_details().getLast_name();
        holder.tv_StoreName.setText(storename);
        holder.tv_Price.setText(model.getAmount_currency()+model.getTotal_amount());
      String name =  model.getOrder_products().get(position).getProduct_details().getProduct_name();
      String weight =  model.getOrder_products().get(position).getProduct_details().getUnit_weight();

        holder.tv_Weight.setText(name + " "+ "(" +weight + ")");



        DateFormat theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = theDateFormat.parse(model.getCreated_at());
        } catch (ParseException parseException) {
// Date is invalid. Do what you want.
        } catch (Exception exception) {
// Generic catch. Do what you want.
        }

        theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String[] arrayString = model.getCreated_at().split(";");

        String time = arrayString[0];

        time = time.substring(time.indexOf("2020-10-12T") + 12, time.length());
// tvDate.setText(theDateFormat.format(date));
        DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = null;
        try {
            d = f1.parse(time)
            ;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("hh:mm a");
        String timeFormate=f2.format(d).toLowerCase(); // "12:18am"
       // tvD.setText(timeFormate+"h ago");
        holder.tv_DateTime.setText(theDateFormat.format(date)+" "+timeFormate);

        holder.btn_reOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String orderId = String.valueOf(model.getOrderId());
                Toast.makeText(context, orderId, Toast.LENGTH_SHORT).show();
//                Intent i = new Intent(context, AddMoreActivity.class);
//                Utilities.saveString(context,"orderId",orderId);
//                context.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_StoreName,tv_Price,tv_Weight,tv_DateTime;
        private Button btn_reOrder;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_StoreName = itemView.findViewById(R.id.tv_store_name_past);
            tv_Price = itemView.findViewById(R.id.tv_price_past);
            tv_Weight = itemView.findViewById(R.id.tv_weight_past);
            tv_DateTime = itemView.findViewById(R.id.tv_dateTime_past);
            btn_reOrder = itemView.findViewById(R.id.btn_reorder);


        }
    }
}
