package com.mtech.yunnriapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.HomeActivity;
import com.mtech.yunnriapp.activities.HomeMainCategoryFragment;
import com.mtech.yunnriapp.fragments.HomeFragment;
import com.mtech.yunnriapp.models.CategoryDataModel;
import com.mtech.yunnriapp.models.SubCategoryModel;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {


    private Context context;
    private List<CategoryDataModel> list;
    int category_id;
    String cat_name,cat_image;



    public CategoryAdapter(Context context, List<CategoryDataModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_cat_layout_new,parent,false);
        return  new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CategoryDataModel model = list.get(position);
        holder.category.setText(model.getCategory_name());
        Glide.with(context).load(RetrofitClientInstance.BASE_URL_IMG+model.getCategory_image()).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cat_name = model.getCategory_name();
                category_id= model.getId();
                cat_image=model.getCategory_image();
//                Intent intent = new Intent(context,HomeActivity.class);
                Utilities.saveInt(context,"category_id",category_id);
                Utilities.saveString(context,"cat_name",cat_name);
                Utilities.saveString(context,"cat_image",cat_image);
//                context.startActivity(intent);
                ((HomeActivity)context).navController.navigate(R.id.action_categoriesFragment_to_homeFragment);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public  class  ViewHolder extends RecyclerView.ViewHolder{

         TextView category,cat_names;
         LinearLayout layout;
         ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);



            imageView = itemView.findViewById(R.id.image_cat);
            category = itemView.findViewById(R.id.text_cat);
            layout =itemView.findViewById(R.id.new_cat);
//            cat_names =itemView.findViewById(R.id.text_cat);

        }
    }
}
