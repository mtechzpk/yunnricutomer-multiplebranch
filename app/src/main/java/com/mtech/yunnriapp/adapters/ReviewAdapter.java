package com.mtech.yunnriapp.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;


import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.models.ReviewModel;
import com.mtech.yunnriapp.models.aboutInfo.AllReviewDataModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

    List<AllReviewDataModel> list;
    Context context;

    public ReviewAdapter(List<AllReviewDataModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @NonNull
    @Override
    public ReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_item, parent, false);
        return new ReviewAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewAdapter.ViewHolder holder, int position) {
        AllReviewDataModel model = list.get(position);
        String date = model.getCreated_at();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss");
        Date parsedDate = null;
        try {
            parsedDate = inputFormat.parse(date);
            String formattedDate = outputFormat.format(parsedDate);
            holder.date.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.name.setText(model.getReviewed_by_name());
        holder.food.setRating(Float.parseFloat(model.getFood_rating()));
        holder.delivery.setRating(Float.parseFloat(model.getDelivery_rating()));


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public RatingBar food, delivery;
        public TextView name, date;

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            name = itemView.findViewById(R.id.name);
            date = itemView.findViewById(R.id.date);
            food = itemView.findViewById(R.id.ratingFood);
            delivery = itemView.findViewById(R.id.ratingDelivery);


        }
    }
}
