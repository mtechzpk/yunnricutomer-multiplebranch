package com.mtech.yunnriapp.Server;

import com.mtech.yunnriapp.networks.RetrofitClientInstance;

public class Server {

    public static final String BASE_URL = RetrofitClientInstance.BASE_URL;
//    public static final String BASE_URL = "http://nin9teens.com/yuunri_app/public/api/";
//    public static final String BASE_URL = "http://yuunri.com/public/api/";

    public static final String stores_by_category = BASE_URL + "stores_by_category";
    public static final String stores_by_subcategory = BASE_URL + "stores_by_subcategory";
    public static final String get_categories_subcategories = BASE_URL + "get_categories_subcategories";
    public static final String get_categories_by_stores = BASE_URL + "get_categories_by_stores";
    public static final String get_subcategories_by_stores = BASE_URL + "get_subcategories_by_stores";
    public static final String get_products_by_stores = BASE_URL + "get_products_by_stores";
    public static final String products_by_store = BASE_URL + "products_by_store";
    public static final String get_slider_images = BASE_URL + "get_slider_images";
    public static final String get_my_coupons = BASE_URL + "get_my_coupons";
    public static final String get_notifications = BASE_URL + "get_notifications";

}
