package com.mtech.yunnriapp.networks;

import com.google.gson.JsonObject;
import com.mtech.yunnriapp.models.AddressResponseModel;
import com.mtech.yunnriapp.models.CheckOutResponseModel;
import com.mtech.yunnriapp.models.OrderResponseModel;
import com.mtech.yunnriapp.models.PaymentResponseModel;
import com.mtech.yunnriapp.models.ProductResponseModel;
import com.mtech.yunnriapp.models.StoreResponseModel;
import com.mtech.yunnriapp.models.CategoryResponseModel;
import com.mtech.yunnriapp.models.UserResponseModel;
import com.mtech.yunnriapp.models.aboutInfo.AboutInfoResponseModel;
import com.mtech.yunnriapp.models.sliderImages.SliderResponseModel;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GetDateService {

    //Register User
    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("create_customer")
    Call<UserResponseModel> RegisterUser(@Field("first_name") String first_name,
                                         @Field("last_name") String last_name,
                                       @Field("email") String email,
                                       @Field("phone") String phone,
                                       @Field("password") String password,
                                       @Field("password_confirmation") String password_confirmation
    );

    //Login User

    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("logon_customer")
    Call<UserResponseModel> loginUser ( @Field("email") String email,
                                        @Field("password") String password,
                                        @Field("token") String token
    );


    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("about_store")
    Call<AboutInfoResponseModel> about_store(@Field("store_id") String email
    );


    //Update User Profile

@Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("update_profile")
    Call<UserResponseModel> updateProfile ( @Field("name") String name,
                                            @Field("email") String email,
                                            @Field("phone") String phone,
                                            @Field("user_id") String user_id
    );

    // Get Products by Store

    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("get_products_by_stores")
    Call<ProductResponseModel> getProducts (
            @Field("store_id") String  store_id,
            @Field("category_id") String  category_id,
            @Field("subcategory_id") String subcategory_id

    );


    // Get Products by Store

    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("get_subcategories_by_stores")
    Call<ProductResponseModel> get_subcategories_by_stores (
            @Field("store_id") String  store_id,
            @Field("category_id") String  category_id

    );

    //Get All Categories and Sub categories

    @GET("get_categories_subcategories")
    Call<CategoryResponseModel>  getAllCategories();

    @GET("get_slider_images")
    Call<SliderResponseModel>  get_slider_images();



    // Get My All Addresses
    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("get_my_addresses")
    Call<AddressResponseModel> getMyAddress (@Field("user_id") String user_id
    );


    //Delete Address
    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("delete_address")
    Call<AddressResponseModel> deleteAddress (@Field("address_id") String address_id
    );

    // Add new Address

 @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("add_address")
    Call<AddressResponseModel> addNewAddress (@Field("user_id") String user_id,
                                              @Field("street_and_house_number") String street_and_house_number,
                                              @Field("floor") String floor,
                                              @Field("address_type") String address_type,
                                              @Field("latitude") String latitude,
                                              @Field("longitude") String longitude
    );

    //Edit Address

    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("edit_address")
    Call<AddressResponseModel> editAddress (@Field("address_id") String address_id,
                                              @Field("street_and_house_number") String street_and_house_number,
                                              @Field("floor") String floor,
                                              @Field("address_type") String address_type,
                                              @Field("latitude") String latitude,
                                              @Field("longitude") String longitude
    );

    // My Current Orders
    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("my_current_orders")
    Call<OrderResponseModel> currentOrders (@Field("user_id") String user_id
    );


    //My Past Orders
 @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("my_past_orders")
    Call<OrderResponseModel> pastOrders (@Field("user_id") String user_id
    );

 // ReOredr

 @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("reorder")
    Call<OrderResponseModel> reOrder (@Field("order_id") String order_id
 );

 // Add Payment Method
 @Headers({
         "Accept: application/json",
 })

 @FormUrlEncoded
 @POST("add_payment_method")
 Call<PaymentResponseModel> addPaymentCard (@Field("user_id") String user_id,
                                         @Field("name_on_card") String name_on_card,
                                         @Field("card_number") String card_number,
                                         @Field("card_expiry_date") String card_expiry_date,
                                         @Field("card_cvv") String card_cvv
 );

    @Headers({
            "Accept: application/json",
    })
    @POST("checkout")
    Call<CheckOutResponseModel> checkout(@Body JsonObject params);


    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("update_profile_image")
    Call<UserResponseModel> updateProfileImage(@Field("user_id") String user_id,
                                               @Field("image") String image
    );
}
