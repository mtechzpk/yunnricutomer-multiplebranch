package com.mtech.yunnriapp.networks;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

        private static Retrofit retrofit;
//        private static final String BASE_URL = "http://yuunri.com/api/";
        public static final String BASE_URL = "http://yuunri.com/yuunri_apis/public/api/";
//        public static final String BASE_URL = "http://yuunri.com/public/api/";
        public static final String BASE_URL_IMG = "http://yuunri.com/yuunri_apis/public/";
//        public static final String BASE_URL_IMG = "http://yuunri.com/public/";

//        private static final String BASE_URL = "http://nin9teens.com/yuunri/public/api/";
//        public static final String BASE_URL_IMG = "http://nin9teens.com/yuunri/public/";

        public static Retrofit getRetrofitInstance() {
            if (retrofit == null) {
                retrofit = new retrofit2.Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }
}
