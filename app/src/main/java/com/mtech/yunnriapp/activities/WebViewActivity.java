package com.mtech.yunnriapp.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.utils.Utilities;

public class WebViewActivity extends AppCompatActivity {
    WebView webView;
    ImageView img_backarrow;
    TextView tv_activityName;
    String url="";
    ProgressBar progressBar;
    int PIC_WIDTH = 360;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_web_view);

        init();
        if (!Utilities.isNetworkConnected(WebViewActivity.this))
            Utilities.makeToast (getApplicationContext (),"Kindly Connect Your Internet");
        else loadUrl(url);


        img_backarrow.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
    }

    private void loadUrl(String url) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        //webView.getSettings().setPluginsEnabled(true);
        //webView.setWebViewClient(new HelloWebViewClient());
        webView.setWebViewClient(new WebViewClient () {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(0);
            }

            public void onPageFinished(WebView view, String url) {

                progressBar.setVisibility(View.GONE);
                progressBar.setProgress(100);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                Toast.makeText(getApplicationContext (), "Something went wrong!", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
        webView.setWebChromeClient(new WebChromeClient () {
            public void onProgressChanged(WebView view, int progress) {

                progressBar.setProgress(progress); //Make the bar disappear after URL is loaded
                if (progress == 100) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
        webView.loadUrl(url);
        //webView.loadUrl("http://www.examplefoo.com");
    }
    private void init() {
        webView=findViewById (R.id.webView);
        //WebView web = new WebView(this);
        webView.setPadding(0, 0, 0, 0);
        webView.setInitialScale(getScale());
        progressBar=findViewById (R.id.progressBar);
        img_backarrow=findViewById (R.id.img_backarrow);
        tv_activityName=findViewById (R.id.tv_activityName);

        Intent intent = getIntent();
        url=intent.getStringExtra("URL");
        String activityName=intent.getStringExtra ("Title");
        tv_activityName.setText (activityName);
    }
    private int getScale(){
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = new Double(width)/new Double(PIC_WIDTH);
        val = val * 100d;
        return val.intValue();
    }
}