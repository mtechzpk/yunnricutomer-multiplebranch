package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.Cart;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.adapters.OrdersDetailsAdapter;
import com.mtech.yunnriapp.models.OrderDetailsModel;

import java.util.ArrayList;
import java.util.List;

public class OrdersDeliveredActivity extends AppCompatActivity {

    private RecyclerView orderDetailsRecyclerview;
    private OrdersDetailsAdapter ordersDetailsAdapter;
    private List<Cart> list;
    private LinearLayoutManager manager;

    private RelativeLayout top_layout;
    private ImageView back_ic;
    private Button btn_Continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_delivered);

        top_layout = findViewById(R.id.layout_top);
        back_ic = findViewById(R.id.back_ic);
        btn_Continue = findViewById(R.id.btn_Continue);

        MyDatabase database  = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "My_Cart").allowMainThreadQueries().build();


        btn_Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                database.cartDao().deleteAll();
                startActivity(new Intent(OrdersDeliveredActivity.this,HomeActivity.class));
                finish();
            }
        });




        orderDetailsRecyclerview = findViewById(R.id.order_deatils_RecyclerView);
        orderDetailsRecyclerview.setHasFixedSize(true);
        manager = new LinearLayoutManager(this);
        orderDetailsRecyclerview.setLayoutManager(manager);
        list = database.cartDao().getAllCartItem();


        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        top_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                return;
            }
        });

//

        ordersDetailsAdapter =new OrdersDetailsAdapter(this,list);
        orderDetailsRecyclerview.setAdapter(ordersDetailsAdapter);

    }
}