package com.mtech.yunnriapp.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.adapters.AboutAdapter;

public class AboutActivity extends AppCompatActivity {


    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.viewPager);

        AboutAdapter aboutAdapter = new AboutAdapter(getSupportFragmentManager());
        viewPager.setAdapter(aboutAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
}