package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.mtech.yunnriapp.models.AddressResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewAddressActivity extends AppCompatActivity  implements OnMapReadyCallback {

    private ImageView back_ic;
    private Button btn_save;

    private GoogleMap locationmap;
    FusedLocationProviderClient fusedLocationProviderClient;
    static public Location currenlocation;
    private static final int REQUEST_CODE = 101;

    EditText ed_streetNo,ed_unitNo;
    RadioGroup radioGroup;

    KProgressHUD progressHUD;
    String latitude,longitude;

    String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);

        btn_save = findViewById(R.id.btn_save);
        back_ic = findViewById(R.id.back_ic);

        ed_streetNo = findViewById(R.id.ed_housNO_new);
        ed_unitNo = findViewById(R.id.ed_unit_no_new);
        radioGroup = findViewById(R.id.rgSelect_new);
        userId = String.valueOf(Utilities.getInt(getApplicationContext(),"userID"));

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton button = radioGroup.findViewById(checkedId);
            }
        });


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String streetNo = ed_streetNo.getText().toString();
                String floor = ed_unitNo.getText().toString();
                int selectedId = radioGroup.getCheckedRadioButtonId();
                if (selectedId == -1) {
                    Toast.makeText(AddNewAddressActivity.this,
                            "Select a Label",
                            Toast.LENGTH_SHORT)
                            .show();
                } else {

                    RadioButton radioButton
                            = (RadioButton) radioGroup
                            .findViewById(selectedId);
                    String addressType = radioButton.getText().toString();

                    AddnewAddress(userId,streetNo,floor,addressType,latitude,longitude);
                }
            }
        });

        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLastLxocation();
    }

    private void AddnewAddress(String userId, String streetNo, String floor, String addressType, String latitude, String longitude) {

        progressHUD = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();


        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<AddressResponseModel> call = service.addNewAddress(userId,streetNo,floor,addressType,latitude,longitude);

        call.enqueue(new Callback<AddressResponseModel>() {
            @Override
            public void onResponse(Call<AddressResponseModel> call, Response<AddressResponseModel> response) {
                progressHUD.dismiss();
                int status = response.body().getStatus();
                if (status == 200 ){

                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(),AddressesActivity.class);
                    startActivity(intent);

                }
            }

            @Override
            public void onFailure(Call<AddressResponseModel> call, Throwable t) {
                progressHUD.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void fetchLastLxocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(this ,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_CODE);;


            return;
        }


        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if(location != null){

                    currenlocation = location;
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googlemap);
                    supportMapFragment.getMapAsync(AddNewAddressActivity.this);
                }

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        locationmap = googleMap;
        LatLng latLng = new LatLng(currenlocation.getLatitude() ,currenlocation.getLongitude());
        Toast.makeText(this, latLng+"", Toast.LENGTH_SHORT).show();
        latitude= String.valueOf(currenlocation.getLatitude());
        longitude= String.valueOf(currenlocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        markerOptions.title("Here");
        locationmap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        locationmap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
        locationmap.addMarker(markerOptions);

    }
}