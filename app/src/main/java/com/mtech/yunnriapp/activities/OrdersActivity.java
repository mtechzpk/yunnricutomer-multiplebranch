package com.mtech.yunnriapp.activities;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.adapters.OrderTabAdapter;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class OrdersActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView img_backarrow;
    TabLayout tabLayout;
    ViewPager viewPagerHomeTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        init();
    }

    private void init() {
        img_backarrow = findViewById(R.id.img_backarrow);
        img_backarrow.setOnClickListener(this);

        tabLayout = findViewById(R.id.home_tabLayout);
        viewPagerHomeTabs = findViewById(R.id.viewPagerHomeTabs);
        final OrderTabAdapter myPagerAdapter = new OrderTabAdapter
                (getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPagerHomeTabs.setAdapter(myPagerAdapter);
        tabLayout.setupWithViewPager(viewPagerHomeTabs);
        setMarginBetweenTabs(tabLayout);
    }

    public void setMarginBetweenTabs(TabLayout tabLayout) {

        ViewGroup tabs = (ViewGroup) tabLayout.getChildAt(0);
        for (int i = 0; i < tabs.getChildCount() - 1; i++) {
            View tab = tabs.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            layoutParams.setMarginEnd(-1);
            tab.setLayoutParams(layoutParams);
            tabLayout.requestLayout();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_backarrow:
                finish();
            default:

        }
    }

    @Override
    protected void onResume() {
        init();
        super.onResume();
    }
}