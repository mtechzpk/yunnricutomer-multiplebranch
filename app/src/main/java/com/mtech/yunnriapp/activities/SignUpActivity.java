package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.models.UserResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {


    private ImageView back_ic;
    private EditText ed_firstname,ed_lastname,ed_email,ed_phonenumber;
    private ShowHidePasswordEditText ed_password,ed_confirm_password;
    private Button btn_signup;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        back_ic = findViewById(R.id.back_ic);
        ed_firstname = findViewById(R.id.sign_firstname);
        ed_lastname = findViewById(R.id.sign_lastname);
        ed_email = findViewById(R.id.sign_email);
        ed_password = findViewById(R.id.sign_Password);
        ed_confirm_password = findViewById(R.id.sign_confirm_Password);
        ed_phonenumber = findViewById(R.id.sign_phonenumber);
        btn_signup = findViewById(R.id.btn_sigUp);

        progressDialog = new ProgressDialog(this, AlertDialog.THEME_HOLO_DARK);
        progressDialog.setMessage("Creating User...");


        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog.show();
                String fname = ed_firstname.getText().toString();
                String lname = ed_lastname.getText().toString();
                String email = ed_email.getText().toString();
                String phone = ed_phonenumber.getText().toString();
                String password = ed_password.getText().toString();
                String cpassword = ed_confirm_password.getText().toString();




                if (fname.isEmpty() ||lname.isEmpty() || email.isEmpty() || phone.isEmpty() || password.isEmpty() || cpassword.isEmpty() || !password.matches(cpassword))
                {
                    progressDialog.dismiss();
                    Toast.makeText(SignUpActivity.this, "Please Enter Missing Fields or Correct Password.", Toast.LENGTH_SHORT).show();
                }

                else
                {
                   createUser(fname,lname,email,phone,password,cpassword);
                }

          }

        });

        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void createUser(String fname,String lname,String email, String phone,  String password, String cpassword) {

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<UserResponseModel> call = service.RegisterUser(fname,lname,email,phone,password,cpassword);

        call.enqueue(new Callback<UserResponseModel>() {
            @Override
            public void onResponse(Call<UserResponseModel> call, Response<UserResponseModel> response) {
                progressDialog.dismiss();
                int status = response.body().getStatus();
                if (status == 200) {
                    Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                    Intent signIntent = new Intent(getApplicationContext(), LoginActivity.class);


                    startActivity(signIntent);
                    finish();
                } else if (status == 400) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();

            }
        });

    }

    public  void  SignIn (View v){

        startActivity(new Intent(SignUpActivity.this,LoginActivity.class));

    }

    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(this);
        pDialog
                .setTitle("Exit")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.ic_baseline_exit_to_app_24)
                .setIconTint(R.color.color_Topbar_yellow)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.color_Topbar_yellow,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finish();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.black,
                        R.color.grey,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

}