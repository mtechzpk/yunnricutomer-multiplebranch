package com.mtech.yunnriapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.fragments.CurrentFragment;
import com.mtech.yunnriapp.fragments.PastFragment;

import java.util.ArrayList;

public class VochersActivity extends AppCompatActivity {

    private ViewPager viewpager;
    private TabLayout tabLayout;
    private ImageView back_ic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vochers);

        back_ic = findViewById(R.id.back_ic);
        viewpager =  findViewById(R.id.viewpager);
        tabLayout =  findViewById(R.id.tabs);

       back_ic.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CurrentFragment(),"Current");
        adapter.addFragment(new PastFragment(), "Past");



        viewpager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewpager);
    }






    public class ViewPagerAdapter extends FragmentPagerAdapter{

        private ArrayList<Fragment> fragments;
        private ArrayList<String> titles;

        public ViewPagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
            this.fragments= new ArrayList<>();
            this.titles = new ArrayList<>();
        }


        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position)  ;
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
        public  void addFragment(Fragment fragment, String title){
            fragments.add(fragment);
            titles.add(title);

        }
        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }


    }
}