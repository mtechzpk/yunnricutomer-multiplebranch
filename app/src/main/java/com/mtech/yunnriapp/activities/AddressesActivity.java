package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.adapters.AddressesAdapter;
import com.mtech.yunnriapp.models.AddressResponseModel;
import com.mtech.yunnriapp.models.AddressesModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressesActivity extends AppCompatActivity {


    private ImageView back_ic;

    private LinearLayout add_address_button;

     RecyclerView address_recyclerview;
     AddressesAdapter adapter;
     LinearLayoutManager manager;
     List<AddressesModel> list;
     KProgressHUD hud;

     String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addresses);

        back_ic = findViewById(R.id.back_ic);
        add_address_button = findViewById(R.id.add_address_button);
        address_recyclerview = findViewById(R.id.addresses_recyclerview);


        userId = String.valueOf(Utilities.getInt(getApplicationContext(),"userID"));


        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();


        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<AddressResponseModel> call = service.getMyAddress(userId);
        call.enqueue(new Callback<AddressResponseModel>() {
            @Override
            public void onResponse(Call<AddressResponseModel> call, Response<AddressResponseModel> response) {
                hud.dismiss();
                int status = response.body().getStatus();
                list = response.body().getData();
                if (status == 200){
                    showMyAddress(address_recyclerview,list);
                }
            }

            @Override
            public void onFailure(Call<AddressResponseModel> call, Throwable t) {
                hud.dismiss();
                t.printStackTrace();
            }
        });



        add_address_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AddressesActivity.this,AddNewAddressActivity.class));
                finish();
            }
        });

        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void showMyAddress(RecyclerView address_recyclerview, List<AddressesModel> list) {
        manager = new LinearLayoutManager(this);
        address_recyclerview.setHasFixedSize(true);
        address_recyclerview.setLayoutManager(manager);
        adapter = new AddressesAdapter(this, list);
        address_recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}