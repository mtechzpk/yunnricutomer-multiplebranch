package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mtech.yunnriapp.CartClickListner;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.Cart;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.adapters.CartAdapter;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

public class AddMoreActivity extends AppCompatActivity implements CartClickListner {

    public static MyDatabase myDatabase;

    private ImageView cross_ic;
    TextView tv_deliverCost,deliveryCostunder;
    private RecyclerView addCart_Recyclerview;
    private CartAdapter cartAdapter;
    private List<Cart> itemList;
    public  static TextView subtotal;
    TextView  tv_total, tv_text, tv_min_Orderprice, tvAddMore;
    private LinearLayoutManager manager;
    float totalPrices;
    String minPurchase, prodcut_currency;
    RelativeLayout bottomLayout;
    private double total;
    private int total_bill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_more);

        cross_ic = findViewById(R.id.cross_ic);
        tv_deliverCost = findViewById(R.id.tv_deliverCost);
        deliveryCostunder = findViewById(R.id.deliveryCostunder);
        subtotal = findViewById(R.id.tv_subTotal);
        bottomLayout = findViewById(R.id.bottom_layout_cart);
        tv_text = findViewById(R.id.tv_text);
        tv_total = findViewById(R.id.tv_total);
        tv_min_Orderprice = findViewById(R.id.tv_min_Orderprice);
        tvAddMore = findViewById(R.id.tvAddMore);

        // Utilities

        minPurchase = Utilities.getString(AddMoreActivity.this, "minPurchase");
        prodcut_currency = Utilities.getString(getApplicationContext(), "prodcut_currency");

        tv_deliverCost.setText("0.0"+prodcut_currency);
        deliveryCostunder.setText("Delivery Cost (Under 50.00"+prodcut_currency+")");


        myDatabase = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "My_Cart").allowMainThreadQueries().build();
        getCartitems();

        cross_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        bottomLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemList.size() > 0){

                    Intent intent = new Intent(getApplicationContext(),CheckOutActivity.class);
                    intent.putExtra("total_amount",String.valueOf(total));
                    startActivity(intent);
                }else {

                    Toast.makeText(AddMoreActivity.this, "Can't checkout with empty cart", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void getCartitems() {

        total = 0.0;
        itemList = new ArrayList<>();
        itemList = myDatabase.cartDao().getAllCartItem();
        addCart_Recyclerview = findViewById(R.id.cart_recyclerview);
        addCart_Recyclerview.setHasFixedSize(true);
        manager = new LinearLayoutManager(getApplication());
        addCart_Recyclerview.setLayoutManager(manager);
        cartAdapter = new CartAdapter(itemList, getApplication(),AddMoreActivity.this);
        addCart_Recyclerview.setAdapter(cartAdapter);

        if (itemList.size()>0){

            calculatebill();

        }else {

            subtotal.setText("$ 0.00");
            tv_total.setText("$ 0.00");
            tv_text.setText("Ckeckout 0.00 $");
            tv_min_Orderprice.setText("0.00 $");
            tvAddMore.setVisibility(View.GONE);

        }
    }

    private void calculatebill() {

        for (int i = 0; i <= itemList.size()-1; i++){

            total = total + (Double.parseDouble(itemList.get(i).getPrice()) * Double.parseDouble(itemList.get(i).getQuantity()));
            subtotal.setText("$"+" "+String.valueOf(total));
            tv_total.setText("$"+" "+String.valueOf(total));
            tv_text.setText("Ckeckout "+ String.valueOf(total)+" "+"$");

            double min_store_price = Double.parseDouble(Utilities.getString(AddMoreActivity.this,"storeMinPrice"));
            if (total >= min_store_price){

                tv_min_Orderprice.setText(String.valueOf("0.0 $"));
                tvAddMore.setVisibility(View.GONE);
            }else {

                double less_amount = min_store_price - total;
                tv_min_Orderprice.setText(String.valueOf(less_amount+" $"));
                tvAddMore.setVisibility(View.VISIBLE);


            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();


    }
    public void AddMore(View view) {
        Intent intent = new Intent(getApplicationContext(), Home1Activity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onPlusClick(Cart cartItem) {

       getCartitems();

    }

    @Override
    public void onMinusClick(Cart cartItem) {

        getCartitems();


    }

    @Override
    public void onDeleteClick(Cart cartItem) {

        getCartitems();

    }
}
