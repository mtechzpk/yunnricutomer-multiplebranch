package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.opengl.Visibility;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.FavStoreModel;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.adapters.FavouriteStoresAdapter;

import java.util.List;

public class FavouriteStoresActivity extends AppCompatActivity {

    private ImageView  back_ic;

   private RecyclerView FavouriteStores_recyclerview;
   private FavouriteStoresAdapter favouriteStoresAdapter;
   private GridLayoutManager manager;
   private List<FavStoreModel> list;
   MyDatabase database;
   TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_stores);

        back_ic = findViewById(R.id.back_ic);
        textView = findViewById(R.id.notFound);

        database = Room.databaseBuilder(getApplicationContext(),MyDatabase.class,"MyFav").allowMainThreadQueries().build();
        list = database.cartDao().getAllFav();

        FavouriteStores_recyclerview  = findViewById(R.id.FavouriteStores_recyclerview);
        FavouriteStores_recyclerview.setHasFixedSize(true);
        manager = new GridLayoutManager(this,2);
        FavouriteStores_recyclerview.setLayoutManager(manager);

        if (!list.isEmpty())
        {
            favouriteStoresAdapter = new FavouriteStoresAdapter(this,list);
            FavouriteStores_recyclerview.setAdapter(favouriteStoresAdapter);

        }
        else {
            textView.setVisibility(View.VISIBLE);
            FavouriteStores_recyclerview.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "No Favourite", Toast.LENGTH_SHORT).show();
        }
        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}