package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.models.UserResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactInfoActivity extends AppCompatActivity {

    private ImageView back_ic;

    LinearLayout layout_details,layout_edit_details;
    String name,email,phone,userId,nameEdited,emailEdited,phoneEdited;

    EditText ed_name,ed_email,ed_phone;
    TextView textView_name,textView_phone,textView_mail;
    Button btn_edit,btn_save;
    KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info);

        back_ic = findViewById(R.id.back_ic);
        btn_edit = findViewById(R.id.btn_edit);
        btn_save = findViewById(R.id.btn_save);

        ed_email = findViewById(R.id.ed_email_edit);
        ed_name = findViewById(R.id.ed_name_edit);
        ed_phone = findViewById(R.id.ed_phone_edit);


        textView_mail = findViewById(R.id.tv_gmail_edit);
        textView_name = findViewById(R.id.tv_name_edit);
        textView_phone = findViewById(R.id.tv_number_edit);


        layout_edit_details = findViewById(R.id.layout_edit_details);
        layout_details = findViewById(R.id.layout_details);

        name  = Utilities.getString(getApplicationContext(),"userName");
        email =  Utilities.getString(getApplicationContext(),"userEmail");
        phone =  Utilities.getString(getApplicationContext(),"userPhone");
        userId = String.valueOf(Utilities.getInt(getApplicationContext(),"userID"));


        textView_name.setText(name);
        textView_mail.setText(email);
        textView_phone.setText(phone);


        ed_phone.setText(phone);
        ed_name.setText(name);
        ed_email.setText(email);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                layout_edit_details.setVisibility(View.VISIBLE);
                layout_details.setVisibility(View.GONE);
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                String editName = ed_name.getText().toString();
                String editEmail = ed_email.getText().toString();
                String editPhone = ed_phone.getText().toString();

                editUserInfo(userId,editName,editEmail,editPhone);

                layout_details.setVisibility(View.VISIBLE);
                layout_edit_details.setVisibility(View.GONE);



            }
        });

//        layout_details  = findViewById(R.id.layout_details);




        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void editUserInfo(String userId, String editName, String editEmail, String editPhone) {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<UserResponseModel> call = service.updateProfile(editName,editEmail,editPhone,userId);

        call.enqueue(new Callback<UserResponseModel>() {
            @Override
            public void onResponse(Call<UserResponseModel> call, Response<UserResponseModel> response) {

                int status = response.body().getStatus();
                String message = response.body().getMessage();

                 nameEdited =response.body().getData().getFirst_name()+" "+response.body().getData().getLast_name();
                 emailEdited =response.body().getData().getEmail();
                 phoneEdited =response.body().getData().getPhone();
                if (status == 200){
                    hud.dismiss();
                    Toast.makeText(ContactInfoActivity.this, message, Toast.LENGTH_SHORT).show();

                    Utilities.saveString(getApplicationContext(),"userName",nameEdited);
                    Utilities.saveString(getApplicationContext(),"userEmail",emailEdited);
                    Utilities.saveString(getApplicationContext(),"userPhone",phoneEdited);


                    textView_name.setText(nameEdited);
                    textView_mail.setText(emailEdited);
                    textView_phone.setText(phoneEdited);


                }

            }

            @Override
            public void onFailure(Call<UserResponseModel> call, Throwable t) {

                hud.dismiss();
                t.printStackTrace();
            }
        });


    }
}