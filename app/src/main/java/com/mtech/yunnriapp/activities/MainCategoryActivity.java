package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.adapters.CategoryAdapter;
import com.mtech.yunnriapp.models.CategoryDataModel;
import com.mtech.yunnriapp.models.CategoryResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainCategoryActivity extends AppCompatActivity {



    private RecyclerView category_recyclerview;
    private CategoryAdapter adpter;
    private StaggeredGridLayoutManager manager;
    KProgressHUD progressHUD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maincategory);
        category_recyclerview = findViewById(R.id.category_recyclerview);
//
//        ImageSlider imageSlider = findViewById(R.id.image_slider);
//
//        List<SlideModel> slideModels = new ArrayList<>();
//        slideModels.add(new SlideModel(R.drawable.food_image, ScaleTypes.CENTER_CROP));
//        slideModels.add(new SlideModel(R.drawable.demo1, ScaleTypes.CENTER_CROP));
//        slideModels.add(new SlideModel(R.drawable.demo2, ScaleTypes.CENTER_CROP));
//        slideModels.add(new SlideModel(R.drawable.demo1, ScaleTypes.CENTER_CROP));
//        imageSlider.setImageList(slideModels,ScaleTypes.CENTER_CROP);

        progressHUD = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        if(!isConnected(this)){
            buildDialog(this).show();
        }
        else {
//            Toast.makeText(context, "Dashboard", Toast.LENGTH_SHORT).show();
        }

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);


        Call<CategoryResponseModel> call = service.getAllCategories();
        call.enqueue(new Callback<CategoryResponseModel>() {
            @Override
            public void onResponse(Call<CategoryResponseModel> call, Response<CategoryResponseModel> response) {

                progressHUD.dismiss();
                int status = response.body().getStatus();
                if (status == 200) if (response.body() != null) {
                    status = response.body().getStatus();
                }
                if (!String.valueOf(status).isEmpty() && status == 200) {
                    List dataList = response.body().getData();
                    if (dataList == null && getApplicationContext() != null) {
                        Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                    } else {
                        getCategories(category_recyclerview, dataList);

                    }
                } else if (status == 400) {
                    progressHUD.dismiss();
                    Toast.makeText(getApplicationContext(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<CategoryResponseModel> call, Throwable t) {
                progressHUD.dismiss();
                t.printStackTrace();

            }
        });

    }

    private void getCategories(RecyclerView category_recyclerview, List dataList) {
        adpter = new CategoryAdapter(this,dataList);
        manager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        category_recyclerview.setHasFixedSize(true);
        category_recyclerview.setLayoutManager(manager);
        category_recyclerview.setAdapter(adpter);

    }

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else
            return false;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        return builder;
    }

    @Override
    public void onBackPressed() {
        showCustomDialog();

    }
    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(this);
        pDialog
                .setTitle("Exit")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.ic_baseline_exit_to_app_24)
                .setIconTint(R.color.color_Topbar_yellow)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.color_Topbar_yellow,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finish();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.black,
                        R.color.grey,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

}