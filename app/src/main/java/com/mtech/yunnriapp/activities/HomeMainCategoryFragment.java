package com.mtech.yunnriapp.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Server.MySingleton;
import com.mtech.yunnriapp.Server.Server;
import com.mtech.yunnriapp.adapters.CategoryAdapter;
import com.mtech.yunnriapp.adapters.ImageSliderAdapter;
import com.mtech.yunnriapp.adapters.MainCategoryAdapter;
import com.mtech.yunnriapp.models.CategoryResponseModel;
import com.mtech.yunnriapp.models.sliderImages.SliderImagesDataModel;
import com.mtech.yunnriapp.models.sliderImages.SliderResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeMainCategoryFragment extends Fragment {
    View view;
    private RecyclerView category_recyclerview;
    private MainCategoryAdapter adpter;
    private StaggeredGridLayoutManager manager;
    KProgressHUD progressHUD;
    ImageSlider imageSlider;
    ArrayList<SliderImagesDataModel> list;
    private List<SliderImagesDataModel> sliderImagemodels;
    ViewPager2 viewPager2;
    private Handler ImageSliderHandler = new Handler();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for getContext() fragment

        view = inflater.inflate(R.layout.activity_maincategory, container, false);
        category_recyclerview = view.findViewById(R.id.category_recyclerview);
        viewPager2 = view.findViewById(R.id.imageSlider_Viewpager);
        progressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        if (!isConnected(getContext())) {
            buildDialog(getContext()).show();
        } else {
//            Toast.makeText(context, "Dashboard", Toast.LENGTH_SHORT).show();
        }

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);


        Call<CategoryResponseModel> call = service.getAllCategories();
        call.enqueue(new Callback<CategoryResponseModel>() {
            @Override
            public void onResponse(Call<CategoryResponseModel> call, Response<CategoryResponseModel> response) {

                progressHUD.dismiss();
                int status = response.body().getStatus();
                if (status == 200) if (response.body() != null) {
                    status = response.body().getStatus();
                }
                if (!String.valueOf(status).isEmpty() && status == 200) {
                    List dataList = response.body().getData();
                    if (dataList == null && getContext() != null) {
                        Toast.makeText(getContext(), "No Record Found", Toast.LENGTH_LONG).show();
                    } else {
                        getCategories(category_recyclerview, dataList);

                    }
                } else if (status == 400) {
                    progressHUD.dismiss();
                    Toast.makeText(getContext(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<CategoryResponseModel> call, Throwable t) {
                progressHUD.dismiss();
                t.printStackTrace();

            }
        });
        getSliderImagesApi();
        return view;
    }


    private void getSliderImagesApi() {
//        hud = KProgressHUD.create(HomeActivity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setCancellable(true)
//                .setAnimationSpeed(2)
//                .setDimAmount(0.5f)
//                .show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.GET, Server.get_slider_images, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    list = new ArrayList<SliderImagesDataModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
//                        hud.dismiss();
                        final JSONArray objUser = object.getJSONArray("data");
                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);
                            String slider_image = jsonObject.getString("slider_image");
                            int id = jsonObject.getInt("id");

                            list.add(new SliderImagesDataModel(slider_image));


                            viewPager2.setAdapter(new ImageSliderAdapter(getActivity(), list, viewPager2));
                            viewPager2.setClipToPadding(false);
                            viewPager2.setClipChildren(false);
                            viewPager2.setOffscreenPageLimit(2);
                            viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

                            CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
                            compositePageTransformer.addTransformer(new MarginPageTransformer(40));
                            compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
                                @Override
                                public void transformPage(@NonNull View page, float position) {


                                    float a = 1 - Math.abs(position);
                                    page.setScaleY(0.85f + a * 0.15f);
                                }
                            });

                            viewPager2.setPageTransformer(compositePageTransformer);
                            viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                                @Override
                                public void onPageSelected(int position) {
                                    super.onPageSelected(position);
                                    ImageSliderHandler.removeCallbacks(imagesliderRunable);
                                    ImageSliderHandler.postDelayed(imagesliderRunable, 3000);
                                }


                            });

                        }
                    } else {
//                        hud.dismiss();
                        String message = object.getString("message");
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
//                    hud.dismiss();
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                hud.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";

                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
//                params.put("do", "area_units");
//                params.put("apikey", "travces.com");

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);


    }

    private void getCategories(RecyclerView category_recyclerview, List dataList) {
        adpter = new MainCategoryAdapter(getContext(), dataList);
        manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        category_recyclerview.setHasFixedSize(true);
        category_recyclerview.setLayoutManager(manager);
        category_recyclerview.setAdapter(adpter);

    }

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(getContext().CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        } else
            return false;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access getContext(). Press ok to Exit");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        return builder;
    }
//
//    @Override
//    public void onBackPressed() {
//        showCustomDialog();
//
//    }
//    private void showCustomDialog() {
//        final PrettyDialog pDialog = new PrettyDialog(getContext());
//        pDialog
//                .setTitle("Exit")
//                .setMessage("Are you sure you want to Exit?")
//                .setIcon(R.drawable.ic_baseline_exit_to_app_24)
//                .setIconTint(R.color.color_Topbar_yellow)
//                .addButton(
//                        "Yes",
//                        R.color.pdlg_color_white,
//                        R.color.color_Topbar_yellow,
//                        new PrettyDialogCallback() {
//                            @Override
//                            public void onClick() {
//                                finish();
//                                pDialog.dismiss();
//                            }
//                        }
//                )
//                .addButton("No",
//                        R.color.black,
//                        R.color.grey,
//                        new PrettyDialogCallback() {
//                            @Override
//                            public void onClick() {
//                                pDialog.dismiss();
//                            }
//                        })
//                .show();

//    }
private Runnable imagesliderRunable = new Runnable() {
    @Override
    public void run() {

        viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
    }
};

    @Override
public void onPause() {
    super.onPause();
    ImageSliderHandler.removeCallbacks(imagesliderRunable);
}

    @Override
    public void onResume() {
        super.onResume();
        ImageSliderHandler.removeCallbacks(imagesliderRunable);
    }
}