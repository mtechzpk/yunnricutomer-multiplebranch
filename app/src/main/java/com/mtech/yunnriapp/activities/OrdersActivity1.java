package com.mtech.yunnriapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.adapters.CurrentOrdersAdapter;
import com.mtech.yunnriapp.adapters.PastOrdersAdapter;
import com.mtech.yunnriapp.models.OrderDataModel;
import com.mtech.yunnriapp.models.OrderResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersActivity1 extends AppCompatActivity {

    private ImageView back_ic;

    private RecyclerView pastordersRecyclerView;
    private List<OrderDataModel> list;
    private LinearLayoutManager manager;


    private RecyclerView currentordersRecyclerView;
    private List<OrderDataModel> currentOrderlist;
    private CurrentOrdersAdapter currentOrdersAdapter;
    private LinearLayoutManager currentordersmanager;
    String userId;
    KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        back_ic = findViewById(R.id.back_ic);
        userId = String.valueOf(Utilities.getInt(getApplicationContext(),"userID"));
        pastordersRecyclerView = findViewById(R.id.orders_rcyclerview);
        currentordersRecyclerView = findViewById(R.id.currentorders_rcyclerview);

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<OrderResponseModel> call = service.pastOrders(userId);
        call.enqueue(new Callback<OrderResponseModel>() {
            @Override
            public void onResponse(Call<OrderResponseModel> call, Response<OrderResponseModel> response) {

                int status = response.body().getStatus();
                list = response.body().getData();
                if (list != null && !list.isEmpty()) {
                    if (status == 200) {
                        hud.dismiss();
                        showPastOrders(list, pastordersRecyclerView);
                    } else {
                        hud.dismiss();
                        Toast.makeText(OrdersActivity1.this, "No Past Order found", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    hud.dismiss();
                    Toast.makeText(OrdersActivity1.this, "No Past Order found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OrderResponseModel> call, Throwable t) {
                hud.dismiss();
                t.printStackTrace();
            }
        });


     getCurrentOrders();


//        call = service.currentOrders(userId);
//        call.enqueue(new Callback<OrderResponseModel>() {
//            @Override
//            public void onResponse(Call<OrderResponseModel> call, Response<OrderResponseModel> response) {
//
//                int status = response.body().getStatus();
//                currentOrderlist = response.body().getData();
//                if (currentOrderlist != null && !currentOrderlist.isEmpty()) {
//                    if (status == 200) {
//                        hud.dismiss();
//                        showcurrentOrders(list, currentordersRecyclerView);
//                    } else {
//                        hud.dismiss();
//                        Toast.makeText(OrdersActivity.this, "No Record Found", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    hud.dismiss();
//                    Toast.makeText(OrdersActivity.this, "no data found", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<OrderResponseModel> call, Throwable t) {
//                hud.dismiss();
//                t.printStackTrace();
//            }
//        });





        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        }

    private void getCurrentOrders() {

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<OrderResponseModel> call = service.currentOrders(userId);
        call.enqueue(new Callback<OrderResponseModel>() {
            @Override
            public void onResponse(Call<OrderResponseModel> call, Response<OrderResponseModel> response) {

                int status = response.body().getStatus();
                currentOrderlist = response.body().getData();
                if (currentOrderlist != null && !currentOrderlist.isEmpty()) {
                    if (status == 200) {
                        showcurrentOrders(currentOrderlist, currentordersRecyclerView);
                    } else {
                        Toast.makeText(OrdersActivity1.this, "No Current Order found", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(OrdersActivity1.this, "No Current Order found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OrderResponseModel> call, Throwable t) {
                Toast.makeText(OrdersActivity1.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });

    }

    private void showcurrentOrders(List<OrderDataModel> list, RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        CurrentOrdersAdapter adapter = new CurrentOrdersAdapter(this,list);
        recyclerView.setAdapter(adapter);
    }


    private void showPastOrders(List<OrderDataModel> list, RecyclerView recyclerView) {

        recyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        PastOrdersAdapter adapter = new PastOrdersAdapter(this,list);
        recyclerView.setAdapter(adapter);
    }
}