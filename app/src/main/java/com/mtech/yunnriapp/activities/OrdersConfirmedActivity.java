package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.Cart;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.adapters.OrdersDetailsAdapter;
import com.mtech.yunnriapp.models.OrderDetailsModel;

import java.util.List;

public class OrdersConfirmedActivity extends AppCompatActivity {

    private RecyclerView orderDetailsRecyclerview;
    private OrdersDetailsAdapter ordersDetailsAdapter;
    private List<Cart> list;
    private LinearLayoutManager manager;

    private RelativeLayout top_layout;
    private ImageView back_ic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_confirmed);

        top_layout = findViewById(R.id.layout_top);
        back_ic = findViewById(R.id.back_ic);

        MyDatabase database  = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "My_Cart").allowMainThreadQueries().build();

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            Intent mainIntent = new Intent(OrdersConfirmedActivity.this, OrdersDeliveredActivity.class);
            startActivity(mainIntent);
            finish();
        }, 5000);


        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });


        orderDetailsRecyclerview = findViewById(R.id.order_deatils_RecyclerView);
        orderDetailsRecyclerview.setHasFixedSize(true);
        manager = new LinearLayoutManager(this);
        orderDetailsRecyclerview.setLayoutManager(manager);
        list = database.cartDao().getAllCartItem();




        top_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                return;
            }
        });


        ordersDetailsAdapter =new OrdersDetailsAdapter(this,list);
        orderDetailsRecyclerview.setAdapter(ordersDetailsAdapter);


    }

    public void back(View view) {
        finish();
    }
}