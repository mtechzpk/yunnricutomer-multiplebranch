package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.Cart;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.Server.MySingleton;
import com.mtech.yunnriapp.Server.Server;
import com.mtech.yunnriapp.models.CheckOutResponseModel;
import com.mtech.yunnriapp.models.coupans.VendorGetCoupanModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOutActivity extends AppCompatActivity {
    Spinner spCounpon;
    private ImageView back_ic;
    private EditText ed_HouseNo, ed_PostCode, ed_City, ed_Floor, ed_CompanyName, ed_Note, ed_FullName, ed_Email, ed_Phonenumber, ed_Votchure;
    private RadioGroup radioGroup;
    TextView textView, tv_text, link;
    private List<Cart> itemList;
    public static MyDatabase myDatabase;
    String cardnum, number;
    int productid, storeId, quantity;
    float price;
    String stttoreId="";
    KProgressHUD hud;
    RelativeLayout Oder_and_Pay;
    int userId;
    int coupanid;
    RadioButton radioButton;
    int selectedId;
    List<String> coupanList;
    String latitude, longitude, prodcut_currency = "";
    CheckBox checkBox;
    String[] category = {"no Coupan"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        prodcut_currency = Utilities.getString(getApplicationContext(), "prodcut_currency");
        stttoreId = String.valueOf(Utilities.getInt(getApplicationContext(), "storeId"));
        back_ic = findViewById(R.id.back_ic);
        ed_HouseNo = findViewById(R.id.ed_housNO_checkout);
        ed_PostCode = findViewById(R.id.ed_postcode_checkout);
        ed_City = findViewById(R.id.ed_city_checkout);
        ed_Floor = findViewById(R.id.ed_Floor_checkout);
        ed_CompanyName = findViewById(R.id.ed_CompanyName_checkout);
        ed_Note = findViewById(R.id.ed_AddNote_checkout);
        ed_FullName = findViewById(R.id.ed_FullName_checkout);
        ed_Email = findViewById(R.id.ed_Email_checkout);
        spCounpon = findViewById(R.id.spCounpon);
        ed_Phonenumber = findViewById(R.id.ed_Phonenumber_checkout);
        tv_text = findViewById(R.id.tv_text);
        radioGroup = findViewById(R.id.radioGroup);
        checkBox = findViewById(R.id.checkbox);
        link = findViewById(R.id.link);
        userId = Utilities.getInt(getApplicationContext(), "userID");
        myDatabase = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "My_Cart").allowMainThreadQueries().build();
        itemList = myDatabase.cartDao().getAllCartItem();


//        Utilities.getString(CheckOutActivity.this,"cardnum");
//        if (number.equals("")||number.isEmpty()||number==null){
//            textView.setText("**** **** **** ****");
//        }
//        else {
//            textView.setText(number);
//        }


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                radioButton = findViewById(checkedId);
            }
        });

        ed_Votchure = findViewById(R.id.ed_voture_checkout);
        String totalAmount = getIntent().getStringExtra("total_amount");
        tv_text.setText("Order And Pay(" + totalAmount + " $)");


        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Oder_and_Pay = findViewById(R.id.bottom_layout);

        Oder_and_Pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String houseNo = ed_HouseNo.getText().toString();
                String postcode = ed_PostCode.getText().toString();
                String city = ed_City.getText().toString();
                String floor = ed_Floor.getText().toString();
                String company = ed_CompanyName.getText().toString();
                String note = ed_Note.getText().toString();
                String fullName = ed_FullName.getText().toString();
                String email = ed_Email.getText().toString();
                String phone = ed_Phonenumber.getText().toString();
                String voucher = ed_Votchure.getText().toString();
                Float longitude = Float.valueOf(Utilities.getString(getApplicationContext(), "current_lat"));
                Float latitude = Float.valueOf(Utilities.getString(getApplicationContext(), "current_lng"));


                if (houseNo.isEmpty() || postcode.isEmpty() || city.isEmpty() || fullName.isEmpty() || email.isEmpty() || phone.isEmpty()) {
                    Toast.makeText(CheckOutActivity.this, "Please Enter all Fields", Toast.LENGTH_SHORT).show();
                } else if (!checkBox.isChecked()) {
                    Toast.makeText(CheckOutActivity.this, "For continue, you have to agree to accept our Privacy Policy & Terms of Service.", Toast.LENGTH_SHORT).show();
                } else {
                    selectedId = radioGroup.getCheckedRadioButtonId();
                    if (selectedId == R.id.cod) {
                        Toast.makeText(CheckOutActivity.this, "Cash", Toast.LENGTH_SHORT).show();
                        callCheckOutApi(totalAmount, houseNo, postcode, city, floor, company, note, fullName, email, phone, String.valueOf(coupanid), latitude, longitude);

                    } else {
                        Toast.makeText(CheckOutActivity.this, "CC", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), AddPaymentMethod.class);
                        startActivity(intent);
                    }
                }

            }
        });

        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "http://yuunri.com/privacy_policy";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        getCoupanLsitApi(stttoreId);
    }

    private void callCheckOutApi(String totalAmount, String houseNo, String postcode, String city, String floor, String company, String note, String fullName, String email, String phone, String voucher, Float latitude, Float longitude) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", userId);
        jsonObject.addProperty("total_amount", totalAmount);
        jsonObject.addProperty("delivery_address", houseNo);
        jsonObject.addProperty("postcode", postcode);
        jsonObject.addProperty("city", city);
        jsonObject.addProperty("floor", floor);
        jsonObject.addProperty("delivery_latitude", latitude);
        jsonObject.addProperty("delivery_longitude", longitude);
        jsonObject.addProperty("company_name", company);
        jsonObject.addProperty("optional_note", note);
        jsonObject.addProperty("full_name", fullName);
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("phone", phone);
        jsonObject.addProperty("payment_method", "cash");
        jsonObject.addProperty("voucher_or_coupon", "none");
        jsonObject.addProperty("delivery_charges", "10");

        JsonArray array = new JsonArray();

        for (int i = 0; i < itemList.size(); i++) {

            productid = itemList.get(i).getId();
            quantity = Integer.parseInt(itemList.get(i).getQuantity());
            price = Float.parseFloat(itemList.get(i).getPrice());
            storeId = itemList.get(i).getStore_id();
            JsonObject object = new JsonObject();
            object.addProperty("product_id", productid);
            object.addProperty("store_id", storeId);
            object.addProperty("quantity", quantity);
            object.addProperty("product_price", price);
            array.add(object);
        }

        jsonObject.add("products", array);

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<CheckOutResponseModel> call = service.checkout(jsonObject);
        call.enqueue(new Callback<CheckOutResponseModel>() {
            @Override
            public void onResponse(Call<CheckOutResponseModel> call, Response<CheckOutResponseModel> response) {


                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
                    hud.dismiss();
                    Toast.makeText(CheckOutActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    myDatabase.cartDao().deleteAll();
                    Intent intent = new Intent(getApplicationContext(), OrdersConfirmedActivity.class);
                    startActivity(intent);
                } else {
                    hud.dismiss();
                    Toast.makeText(CheckOutActivity.this, "Failed" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CheckOutResponseModel> call, Throwable t) {

                hud.dismiss();
                t.printStackTrace();
            }
        });
    }

    public void addPayment(View view) {
        Intent intent = new Intent(getApplicationContext(), AddPaymentMethod.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getCoupanLsitApi(final String store_id) {
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, RetrofitClientInstance.BASE_URL+"get_my_coupons", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    final ArrayList<VendorGetCoupanModel> vendorGetCoupanModels = new ArrayList<VendorGetCoupanModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String message = object.getString("message");

                    if (status == 200) {
                        coupanList = new ArrayList<>();
                        JSONObject jsonObject;
                        JSONArray objUser = object.getJSONArray("data");
                        for (int i = 0; i < objUser.length(); i++) {
                            jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String name = jsonObject.getString("coupon_name");
                            vendorGetCoupanModels.add(new VendorGetCoupanModel(id, name));
                            coupanList.add(name);
                        }
                        // Creating adapter for spinner
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(CheckOutActivity.this,R.layout.spinner_layout, coupanList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_layout);
                        spCounpon.setAdapter(dataAdapter);
//                        subCategory.add(0, "Select Sub Category");
//                        spSubCategory.setSelection(0);
                        spCounpon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String sub_category = parent.getItemAtPosition(position).toString();
                                coupanid = vendorGetCoupanModels.get(position).getId();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } else {
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(CheckOutActivity.this, R.layout.spinner_layout, category);

                        // Drop down layout style - list view with radio button
                        dataAdapter.setDropDownViewResource(R.layout.spinner_layout);

                        // attaching data adapter to spinner
                        spCounpon.setAdapter(dataAdapter);


//                        Toast.makeText(AddProductsActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (CheckOutActivity.this != null)
                    Toast.makeText(CheckOutActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("store_id", store_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(CheckOutActivity.this).addToRequestQueue(RegistrationRequest);


    }
}