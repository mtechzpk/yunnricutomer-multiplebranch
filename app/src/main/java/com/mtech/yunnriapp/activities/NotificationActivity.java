package com.mtech.yunnriapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Server.ApiModelClass;
import com.mtech.yunnriapp.Server.Server;
import com.mtech.yunnriapp.Server.ServerCallback;
import com.mtech.yunnriapp.adapters.NotificationAdapter;
import com.mtech.yunnriapp.models.NotificationModel;
import com.mtech.yunnriapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity {
    RecyclerView rvNotification;
    LinearLayout tvStatus, product_noti;
    ImageView ivBack;
    private List<NotificationModel> notificationModels;
    private NotificationAdapter adapter;
    int store_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_notification);
        store_id = Utilities.getInt(this, "user_id");
        rvNotification = findViewById(R.id.rvNotification);
        tvStatus = findViewById(R.id.tvStatus);
        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getNotificationApi();
    }

    public void getNotificationApi() {
        final IOSDialog dialog0 = new IOSDialog.Builder(NotificationActivity.this)
                .setTitleColorRes(R.color.gray)
                .build();
        dialog0.show();
        Map<String, String> params = new HashMap<String, String>();
        params.put("store_id", String.valueOf("2"));
        HashMap<String, String> headers = new HashMap<String, String>();
        params.put("Accept", "application/json");
        ApiModelClass.GetApiResponse(Request.Method.POST, Server.get_notifications, NotificationActivity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {


                    try {
                        notificationModels = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(result));

                        int status = object.getInt("status");
                        String message = object.getString("message");
                        if (status == 200) {
                            Toast.makeText(NotificationActivity.this, message, Toast.LENGTH_SHORT).show();
                            dialog0.dismiss();
                            JSONArray jsonArray = object.getJSONArray("data");
                            if (jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    int idd = jsonObject.getInt("id");
                                    String notification_from = jsonObject.getString("notification_from");
                                    String notification_to = jsonObject.getString("notification_to");
                                    String notification = jsonObject.getString("notification");
                                    String created_at = jsonObject.getString("created_at");
                                    notificationModels.add(new NotificationModel(idd, notification_from, notification_to, notification, created_at));
                                }


                            } else {

                            }
                            rvNotification.setHasFixedSize(true);
                            rvNotification.setLayoutManager(new GridLayoutManager(NotificationActivity.this, 1, GridLayoutManager.VERTICAL, false));
                            adapter = new NotificationAdapter(NotificationActivity.this, notificationModels);
                            rvNotification.setAdapter(adapter);

                        } else {
                            dialog0.dismiss();
//                            rvNotification.setVisibility(View.GONE);
                            Toast.makeText(NotificationActivity.this, message, Toast.LENGTH_SHORT).show();

                        }
//

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    dialog0.dismiss();
                    Toast.makeText(NotificationActivity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}