package com.mtech.yunnriapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.joooonho.SelectableRoundedImageView;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.FavStoreModel;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.Server.ApiModelClass;
import com.mtech.yunnriapp.Server.Server;
import com.mtech.yunnriapp.Server.ServerCallback;
import com.mtech.yunnriapp.adapters.ProductAdapter;
import com.mtech.yunnriapp.adapters.SubCategoryAdapter;
import com.mtech.yunnriapp.models.CategoriesIDModel;
import com.mtech.yunnriapp.models.CategoryDataModel;
import com.mtech.yunnriapp.models.ProductDataModel;
import com.mtech.yunnriapp.models.ProductResponseModel;
import com.mtech.yunnriapp.models.ProductlistModel;
import com.mtech.yunnriapp.models.SubCategoriesIDModel;
import com.mtech.yunnriapp.models.aboutInfo.AboutInfoResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mtech.yunnriapp.adapters.SubCategoryAdapter.selected_position;
import static com.mtech.yunnriapp.utils.Utilities.getInt;


public class Home1Activity extends AppCompatActivity implements SubCategoryAdapter.Callback {

    private ImageView back_ic, iv_Like, iv_Info, subImage;
    private TextView store_CategoryName, tv_StoreName, tv_reviews;
    private RatingBar RatingsStar;
    SelectableRoundedImageView ProfileImage;
    RecyclerView product_recyclerview;
    ProductAdapter adapter;
    TabLayout cat_tabs, sub_tabs;
    LinearLayoutManager manager;
    KProgressHUD hud;
    List<ProductDataModel> list;
    List<CategoryDataModel> listCat;
    List<ProductlistModel> modelList;
    TextView tv_text;
    int category_id, id, subCatId;
    private ArrayList<CategoriesIDModel> categoriesIDModels;
    private ArrayList<SubCategoriesIDModel> subCategoriesIDModels;
    SubCategoryAdapter adpter;
    String isFav = "";
    String categoryName, storeName, storeImage, subcategoryImage, storeId, subId, totalAmount, storeMinPrice, storeCurrency;
    MyDatabase myDatabase;
    RecyclerView rvSubCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home1);

        list = new ArrayList<>();
        modelList = new ArrayList<>();
        ProfileImage = findViewById(R.id.store_Profileimage);
        store_CategoryName = findViewById(R.id.store_CategoryName);
        rvSubCategory = findViewById(R.id.rvSubCategory);
        iv_Like = findViewById(R.id.iv_like);
        iv_Info = findViewById(R.id.iv_info);
        tv_StoreName = findViewById(R.id.tv1_store_name);
        tv_reviews = findViewById(R.id.tv_reviews);
        RatingsStar = findViewById(R.id.ratings);
        tv_text = findViewById(R.id.tv_text);
        cat_tabs = findViewById(R.id.category_tabLayout);
        subCategoriesIDModels = new ArrayList<>();

        isFav = Utilities.getString(getApplicationContext(), "isFav");
        if (!isFav.isEmpty()) {
//            checkForFavouriteStatus();
        }


        //Utilities
        totalAmount = Utilities.getString(getApplicationContext(), "totalAmount");
        storeMinPrice = Utilities.getString(getApplicationContext(), "storeMinPrice");
        categoryName = Utilities.getString(getApplicationContext(), "subName");
        storeName = Utilities.getString(getApplicationContext(), "storeName");
        storeCurrency = Utilities.getString(getApplicationContext(), "storeCurrency");
        subcategoryImage = Utilities.getString(getApplicationContext(), "subImage");
        storeImage = Utilities.getString(getApplicationContext(), "storeImage");
        storeId = String.valueOf(Utilities.getInt(getApplicationContext(), "storeId"));
        subId = String.valueOf(getInt(getApplicationContext(), "subId"));
        setCatTabTextApi(storeId);
        about_storeApi(storeId);
        cat_tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Fragment fragment = null;

                if (!categoriesIDModels.isEmpty()) {

                    category_id = categoriesIDModels.get(tab.getPosition()).getId();
                    setsubCatTabTextApifirst(String.valueOf(category_id));
                    setsubCatTabTextApi(String.valueOf(category_id));
                    subCategoriesIDModels = new ArrayList<>();

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        //Room Database
        myDatabase = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "MYFav").allowMainThreadQueries().build();
        iv_Like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(Home1Activity.this, iv_Like);
                //inflating menu from xml resource
                popup.inflate(R.menu.delete_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.delete:
                                favourite_statusUpdate();

                                return true;
                            case R.id.info:
                                Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                                startActivity(intent);

                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.show();

            }
        });

//        iv_Like.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                favourite_statusUpdate();
//            }
//        });
        product_recyclerview = findViewById(R.id.product_recyclerview);


        if (totalAmount.equals("null") || totalAmount.equals(null)) {
            tv_text.setText("Basket(0.00)");
        } else {
            tv_text.setText("Basket(" + totalAmount + ")");
        }


        iv_Info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(intent);
            }
        });


        //set views

        Glide.with(this).load(RetrofitClientInstance.BASE_URL_IMG + storeImage).into(ProfileImage);

//        store_CategoryName.setText(categoryName);
        tv_StoreName.setText(storeName);


    }

    private void setsubCatTabTextApifirst(String category_id) {


        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<ProductResponseModel> modelCall = service.get_subcategories_by_stores(storeId, category_id);
        modelCall.enqueue(new Callback<ProductResponseModel>() {
            @Override
            public void onResponse(Call<ProductResponseModel> call, Response<ProductResponseModel> response) {
                hud.dismiss();
                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) if (response.body() != null) {
                    status = response.body().getStatus();
                }
                if (!String.valueOf(status).isEmpty() && status == 200) {
                    List dataList = response.body().getData();
                    if (dataList == null && getApplicationContext() != null) {
                        Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                    } else {
                        getProductsList(product_recyclerview, dataList);

                    }
                } else if (status == 400) {
                    hud.dismiss();
                    Toast.makeText(getApplicationContext(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();

                }
            }


            @Override
            public void onFailure(Call<ProductResponseModel> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }

    public void setCatTabTextApi(String storeId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("store_id", storeId);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, Server.get_categories_by_stores, getApplicationContext(), params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    try {
                        categoriesIDModels = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(result));

                        int status = object.getInt("status");
//                        String message = object.getString("Message");
                        if (status == 200) {
                            final JSONArray objUser = object.getJSONArray("data");
//
                            for (int j = 0; j < objUser.length(); j++) {
//
                                JSONObject jsonObject1 = objUser.getJSONObject(j);

                                String subcategory_name = jsonObject1.getString("category_name");
                                int iid = jsonObject1.getInt("id");
                                Utilities.saveString(Home1Activity.this, "categoryId", String.valueOf(iid));
                                categoriesIDModels.add(new CategoriesIDModel(iid));
//                                tabLayout.addTab(tabLayout.newTab().setText(categoryTitles.get(i).getName()));
                                cat_tabs.addTab(cat_tabs.newTab().setText(subcategory_name));
                            }


                        } else {
//                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(), "Something went Wrong", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Toast.makeText(getApplicationContext(), ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    public void setsubCatTabTextApi(String s) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("category_id", s);
        params.put("store_id", storeId);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, Server.get_subcategories_by_stores, getApplicationContext(), params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    try {

                        JSONObject object = new JSONObject(String.valueOf(result));

                        int status = object.getInt("status");
//                        String message = object.getString("Message");
                        if (status == 200) {
                            final JSONArray array = object.getJSONArray("data");
//

                            for (int j = 0; j < array.length(); j++) {
//
                                JSONObject jsonObject1 = array.getJSONObject(j);

                                String subcategory_name = jsonObject1.getString("subcategory_name");
                                int id = jsonObject1.getInt("id");
                                Utilities.saveString(getApplicationContext(), "categoryId", String.valueOf(id));
                                subCategoriesIDModels.add(new SubCategoriesIDModel(id, subcategory_name));
                            }

                            subCategoriesIDModels.get(0).setSelected(true);
                            adpter = new SubCategoryAdapter(getApplicationContext(), subCategoriesIDModels, Home1Activity.this);
                            manager = new LinearLayoutManager(Home1Activity.this, RecyclerView.HORIZONTAL, false);
                            rvSubCategory.setHasFixedSize(true);
                            rvSubCategory.setLayoutManager(manager);
                            rvSubCategory.setAdapter(adpter);


                        } else {
//                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Toast.makeText(getApplicationContext(), ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    private void favourite_statusUpdate() {

//        if (isFav.equals("false")) {

            //Room Database
            myDatabase = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "MyFav").allowMainThreadQueries().build();
            if (myDatabase.cartDao().isAddtoFav(Integer.parseInt(storeId)) != 1) {
                FavStoreModel model = new FavStoreModel(Integer.parseInt(storeId), storeName, categoryName, storeMinPrice, storeCurrency, storeImage, "true");
                myDatabase.cartDao().insertAllFav(model);
//                iv_Like.setImageResource(R.drawable.ic_baseline_favorite);
                Toast.makeText(this, "Added to Favourite", Toast.LENGTH_SHORT).show();
                Utilities.saveString(getApplicationContext(), "isFav", "true");
            } else {
                Toast.makeText(this, "Already Added to Favourite", Toast.LENGTH_SHORT).show();
            }

//        }
//        else {

//            myDatabase.cartDao().deleteFavItem(Integer.parseInt(storeId));
//            iv_Like.setImageResource(R.drawable.heart_ic);
//            Utilities.saveString(getApplicationContext(), "isFav", "false");
//            Toast.makeText(this, "Removed", Toast.LENGTH_SHORT).show();
//        }
    }
    private void getProductsList(RecyclerView product_recyclerview, List dataList) {

        ProductAdapter adpter = new ProductAdapter(dataList, this);
        manager = new LinearLayoutManager(this);
        product_recyclerview.setHasFixedSize(true);
        product_recyclerview.setLayoutManager(manager);
        product_recyclerview.setAdapter(adpter);
    }

    public void openCart(View view) {
        Intent intent = new Intent(getApplicationContext(), AddMoreActivity.class);
        startActivity(intent);
    }

    public void back(View view) {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        totalAmount = Utilities.getString(getApplicationContext(), "totalAmount");
        if (totalAmount.equals("null") || totalAmount.equals(null)) {
            tv_text.setText("Basket(0.00)");
        } else {
            tv_text.setText("Basket(" + totalAmount + ")");
        }
    }

    public void checkForFavouriteStatus() {
        if (isFav.equals("true")) {
            iv_Like.setImageResource(R.drawable.ic_baseline_favorite);
        } else if (isFav.equals("false")) {
            iv_Like.setImageResource(R.drawable.heart_ic);
        }
    }

    @Override
    public void onItemClick(int position, TextView tvproductName) {

        subCategoriesIDModels.get(position).setSelected(true);
//        tvproductName.setTextColor(getResources().getColor(R.color.color_Topbar_yellow));
        Log.d("YAMM", "onClick : " + position + " " + subCategoriesIDModels.get(position).isSelected());

        for (int i = 0; i < subCategoriesIDModels.size(); i++) {
            if (i != position) {
                subCategoriesIDModels.get(position).setSelected(false);
//                tvproductName.setTextColor(getResources().getColor(R.color.black));
            }
        }
        adpter = new SubCategoryAdapter(getApplicationContext(), subCategoriesIDModels, Home1Activity.this);
        product_recyclerview.setAdapter(adapter);

//        if (adapter != null) {
//            adapter.notifyDataSetChanged();
//        }
//        Log.d("YAMM", "------------- NOTIFY   " + position + "-------------------------------");
//        Toast.makeText(this, "clikced" + subCategoriesIDModels.get(position).getId(), Toast.LENGTH_SHORT).show();
        int subCategory = subCategoriesIDModels.get(position).getId();

        getAllProducts(storeId, String.valueOf(category_id), String.valueOf(subCategory));
    }

    private void getAllProducts(String storeId, String catID, String subID) {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<ProductResponseModel> modelCall = service.getProducts(storeId, catID, subID);
        modelCall.enqueue(new Callback<ProductResponseModel>() {
            @Override
            public void onResponse(Call<ProductResponseModel> call, Response<ProductResponseModel> response) {
                hud.dismiss();
                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) if (response.body() != null) {
                    status = response.body().getStatus();
                }
                if (!String.valueOf(status).isEmpty() && status == 200) {
                    List dataList = response.body().getData();
                    if (dataList == null && getApplicationContext() != null) {
                        Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                    } else {
                        getProductsList(product_recyclerview, dataList);

                    }
                } else if (status == 400) {
                    hud.dismiss();
                    Toast.makeText(getApplicationContext(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<ProductResponseModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void about_storeApi(String storeID) {

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<AboutInfoResponseModel> call = service.about_store(storeID);

        call.enqueue(new Callback<AboutInfoResponseModel>() {
            @Override
            public void onResponse(Call<AboutInfoResponseModel> call, Response<AboutInfoResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
                    Toast.makeText(Home1Activity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    String country = response.body().getData().getAbout_store().getCountry();
                    String state = response.body().getData().getAbout_store().getState();
                    String city = response.body().getData().getAbout_store().getCity();
                    String address = response.body().getData().getAbout_store().getAddress();
                    String zipcode = response.body().getData().getAbout_store().getZipcode();
                    String descriptions = response.body().getData().getAbout_store().getSelf_description();
                    int total_reviews = response.body().getData().getTotal_reviews();
                    float overall_review_score = response.body().getData().getOverall_review_score();
                    if (!TextUtils.isEmpty(String.valueOf(overall_review_score))) {
                        tv_reviews.setText(String.valueOf(total_reviews) + " Reviews");
                    }
                    if (!TextUtils.isEmpty(String.valueOf(total_reviews))) {
                        RatingsStar.setProgress((int) overall_review_score);

                    }
                } else if (status == 400) {
                    Toast.makeText(Home1Activity.this, response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<AboutInfoResponseModel> call, Throwable t) {
                t.printStackTrace();

            }
        });


    }
}