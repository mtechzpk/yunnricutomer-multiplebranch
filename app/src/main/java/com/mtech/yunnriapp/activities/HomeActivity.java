package com.mtech.yunnriapp.activities;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.models.UserResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;
import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {


    private ImageView btn_Cart, btn_menu, btn_search;
    private RelativeLayout top_layout;
    Fragment selectedFragment = null;
    View hView;
    private ClipboardManager myClipboard;
    private ClipData myClip;
    Bitmap u_img1 = null;
    public NavController navController;
    ImageView ivDrawer;
    private DrawerLayout drawer;
    private int RC_LOCATION_ON_REQUEST = 1235;
    private static final int RC_LOCATION_REQUEST = 1234;
    RelativeLayout main_Container;
    Boolean isChange = false;
    private static final float END_SCALE = 0.7f;
    NavigationView navigationView;
    String input = "";
    String deliverTo, itemCounter, userName;
    CircleImageView imageView_header;
    TextView textView_header, textView_deliverTo;
    String userId, image, latitude, longitude;
    KProgressHUD hud;
    TextView text_counter;
    MyDatabase database;
    String support_phone = "", support_email = "";

    BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        main_Container = findViewById(R.id.main_Container);
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
            Utilities.saveString(HomeActivity.this, "current_lat", latitude);
            Utilities.saveString(HomeActivity.this, "current_lng", longitude);
            Toast.makeText(this, "" + Utilities.getString(this, "current_lat") + Utilities.getString(this, "current_lng"), Toast.LENGTH_SHORT).show();

//            Toast.makeText(HomeActivity.this, latitude + " " + longitude, Toast.LENGTH_SHORT).show();
        } else {
            gpsTracker.showSettingsAlert();
        }

        database = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "My_Cart").allowMainThreadQueries().build();


        //Utilities
        userName = Utilities.getString(HomeActivity.this, "userName");
        itemCounter = String.valueOf(Utilities.getInt(HomeActivity.this, "TotalCartItem"));
        userId = String.valueOf(Utilities.getInt(getApplicationContext(), "userID"));
        deliverTo = Utilities.getString(getApplicationContext(), "deliverTo");
        support_phone = Utilities.getString(getApplicationContext(), "support_phone");
        support_email = Utilities.getString(getApplicationContext(), "support_email");


        text_counter = findViewById(R.id.text_counter);
        btn_Cart = findViewById(R.id.btn_cart);
        btn_menu = findViewById(R.id.btn_menu);
        btn_search = findViewById(R.id.btn_search);
        drawer = findViewById(R.id.drawerlayout);
        textView_deliverTo = findViewById(R.id.deliverTo);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        top_layout = findViewById(R.id.layout_top);


        hView = navigationView.getHeaderView(0);
        imageView_header = hView.findViewById(R.id.profileImage);
        textView_header = hView.findViewById(R.id.profileName);


        textView_header.setText(userName);
        textView_deliverTo.setText(deliverTo);

        if (itemCounter.equals("0") || itemCounter.equals(null)) {
            text_counter.setText("0");
        } else {
            text_counter.setText(itemCounter);
        }


        String userProfileImage = Utilities.getString(HomeActivity.this, "profileImage");
        if (userProfileImage.equals("null")) {
            Glide.with(getApplicationContext()).load(R.drawable.profile_ic).placeholder(R.drawable.person_icon_card).into(imageView_header);
        } else {
            Glide.with(getApplicationContext())
                    .load(RetrofitClientInstance.BASE_URL_IMG + userProfileImage)
                    .into(imageView_header);
        }

        imageView_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 100);
            }
        });
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawer,
                R.string.open,
                R.string.closed
        ) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // Scale the View based on current slide offset
                final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                final float offsetScale = 1 - diffScaledOffset;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    main_Container.setScaleX(offsetScale);
                    main_Container.setScaleY(offsetScale);
                }


                // Translate the View, accounting for the scaled width
                final float xOffset = drawerView.getWidth() * slideOffset;
                final float yOffset = drawerView.getHeight() * slideOffset;

                final float xOffsetDiff = main_Container.getWidth() * diffScaledOffset / 2;

                final float xTranslation = xOffset - xOffsetDiff;

                final float yOffsetDiff = main_Container.getHeight() * diffScaledOffset / 15;

                final float yTranslation = yOffset - yOffsetDiff;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    main_Container.setTranslationX(xTranslation);

                }
            }
        };

        drawer.addDrawerListener(mDrawerToggle);

        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        /*Remove navigation drawer shadow/fadding*/
        drawer.setDrawerElevation(0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawer.setElevation(0f);
            main_Container.setElevation(10.0f);
        }
        initNavigation();


        top_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                return;
            }
        });

        btn_Cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, AddMoreActivity.class));
            }
        });

    }

    private void initNavigation() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
        navController = Navigation.findNavController(this, R.id.user_container);
        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
                    if (destination.getLabel().equals("fragment_profile") || destination.getLabel().equals("fragment_notification") || destination.getLabel().equals("fragment_message") || destination.getLabel().equals("fragment_post_an_ad")) {

                    } else {
//                        tvTitle.setText(destination.getLabel());
                    }
                }

            }
        });
        navigationView.getMenu().findItem(R.id.logout).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.setVisible(true);
                showCustomDialog();
                drawer.closeDrawer(GravityCompat.START, false);
                return true;
            }
        });
        navigationView.getMenu().findItem(R.id.help).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                drawer.closeDrawer(GravityCompat.START, false);
                showdialogueForHelp();
                return true;
            }
        });
        navigationView.getMenu().findItem(R.id.FavouriteStores).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(HomeActivity.this, FavouriteStoresActivity.class));

                drawer.closeDrawer(GravityCompat.START, false);
                return true;
            }
        });
        navigationView.getMenu().findItem(R.id.order).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(HomeActivity.this, OrdersActivity.class));
                drawer.closeDrawer(GravityCompat.START, false);
                return true;
            }
        });

        navigationView.getMenu().findItem(R.id.profile).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(HomeActivity.this, ContactInfoActivity.class));
                drawer.closeDrawer(GravityCompat.START, false);
                return true;
            }
        });

        navigationView.getMenu().findItem(R.id.address).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                drawer.closeDrawer(GravityCompat.START, false);
                startActivity(new Intent(HomeActivity.this, AddressesActivity.class));

                return true;
            }
        });

        navigationView.getMenu().findItem(R.id.notifi).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                drawer.closeDrawer(GravityCompat.START, false);
                startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
                return true;
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                imageView_header.setImageBitmap(selectedImage);


                Bitmap immagex = selectedImage;
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                input = imageEncoded;
                input = input.replace("\n", "");
                input = "data:image/png;base64," + input.trim();

                updateProfileImage(userId, input);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    private void updateProfileImage(String userId, String input) {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<UserResponseModel> call = service.updateProfileImage(userId, input);
        call.enqueue(new Callback<UserResponseModel>() {
            @Override
            public void onResponse(Call<UserResponseModel> call, Response<UserResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                String profileImage = response.body().getData().getProfile_image();
                if (status == 200) {
                    hud.dismiss();
                    Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Utilities.saveString(HomeActivity.this, "profileImage", profileImage);
                    if (profileImage != null) {
                        Glide.with(getApplicationContext()).load(RetrofitClientInstance.BASE_URL_IMG + image).placeholder(R.drawable.person_icon_card).into(imageView_header);
                    }
                } else {
                    hud.dismiss();
                    Toast.makeText(HomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponseModel> call, Throwable t) {

                hud.dismiss();
                t.printStackTrace();
            }
        });
    }


    public void goToAddress(View view) {
        Intent intent = new Intent(getApplicationContext(), AddressesActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        itemCounter = String.valueOf(Utilities.getInt(HomeActivity.this, "TotalCartItem"));
        if (itemCounter.equals("0") || itemCounter.equals(null)) {
            text_counter.setText("0");
        } else {
            text_counter.setText(itemCounter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        itemCounter = String.valueOf(Utilities.getInt(HomeActivity.this, "TotalCartItem"));
        if (itemCounter.equals("0") || itemCounter.equals(null)) {
            text_counter.setText("0");
        } else {
            text_counter.setText(itemCounter);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Logout?")
                .setIcon(R.drawable.ic_baseline_delete_forever_24)
                .setIconTint(R.color.color_Topbar_yellow)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.color_Topbar_yellow,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                Utilities.clearSharedPref(getApplicationContext());
                                database.cartDao().deleteAll();
                                Intent logout = new Intent(HomeActivity.this, LoginActivity.class);
                                startActivity(logout);
                                finish();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.black,
                        R.color.grey,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void showdialogueForHelp() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.help_dialog);
        dialog.getWindow().setLayout((int) (width * 0.8), (int) (height * 0.92)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvPhoneNumber = dialog.findViewById(R.id.tvPhoneNumber);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        TextView tvSupportEmaul = dialog.findViewById(R.id.tvSupportEmaul);
        TextView tvPhoneNumberrr = dialog.findViewById(R.id.tvPhoneNumber);
        ImageView tvCopy = dialog.findViewById(R.id.tvCopy);
        tvSupportEmaul.setText(support_email);
        tvPhoneNumberrr.setText(support_phone);
        tvCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                String text;
                myClip = ClipData.newPlainText("text", tvPhoneNumber.getText().toString());
                myClipboard.setPrimaryClip(myClip);

                Toast.makeText(HomeActivity.this, "Phone Number Copied", Toast.LENGTH_SHORT).show();
            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }
    @Override
    public void onBackPressed() {
            if (!navController.getCurrentDestination().getLabel().toString().equals("fragment_home_main_category")) {
                super.onBackPressed();
            if (drawer.isDrawerOpen(GravityCompat.START)){
                drawer.closeDrawers();
            }

        } else {
                showCustomDialogforExit();
            }
    }
    private void showCustomDialogforExit() {
        final PrettyDialog pDialog = new PrettyDialog(HomeActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimaryDark)
                .addButton(
                        "Yes",
                        R.color.colorPrimaryDark,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finishAffinity();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_red,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }
}
