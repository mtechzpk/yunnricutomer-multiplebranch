package com.mtech.yunnriapp.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.models.UserResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button btn_signin;
    private EditText Ed_Email;
    private ShowHidePasswordEditText Ed_Password;
    ProgressDialog progressDialog;
String latitude,longitude;
String newToken="";
   CheckBox checkbox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            newToken = instanceIdResult.getToken();
            Log.e("newToken", newToken);
//            getActivity().getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();
        });
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
            Utilities.saveString(LoginActivity.this, "current_lat", latitude);
            Utilities.saveString(LoginActivity.this, "current_lng", longitude);
            Toast.makeText(this, ""+Utilities.getString(this,"current_lat")+Utilities.getString(this,"current_lng"), Toast.LENGTH_SHORT).show();

//            Toast.makeText(HomeActivity.this, latitude + " " + longitude, Toast.LENGTH_SHORT).show();
        } else {
            gpsTracker.showSettingsAlert();
        }
        btn_signin = findViewById(R.id.btn_signin);
        Ed_Email = findViewById(R.id.ed_login_email);
        Ed_Password = findViewById(R.id.ed_login_password);
        checkbox = findViewById(R.id.checkbox);
        progressDialog = new ProgressDialog(this, AlertDialog.THEME_HOLO_DARK);
        progressDialog.setMessage("Signing In..");

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                
                String email = Ed_Email.getText().toString();
                String password = Ed_Password.getText().toString();

                if (email.isEmpty() || password.isEmpty())
                {
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Please Add Missing Fields", Toast.LENGTH_SHORT).show();
                }else if (!checkbox.isChecked()){
                    Toast.makeText(LoginActivity.this, "For continue, you have to agree to accept our Privacy Policy & Terms of Service.", Toast.LENGTH_SHORT).show();
                }
                else {
                    progressDialog.show();
                    userLogin(email,password);
                }



            }
        });
    }

    private void userLogin(String email, String password) {

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<UserResponseModel> call = service.loginUser(email,password,newToken);

        call.enqueue(new Callback<UserResponseModel>() {
            @Override
            public void onResponse(Call<UserResponseModel> call, Response<UserResponseModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200)
                
                {
                    Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                    String userName = response.body().getData().getFirst_name();
                    String lastName = response.body().getData().getLast_name();

                    int userID = response.body().getData().getId();

                    String userEmail = response.body().getData().getEmail();
                    String userPhone = response.body().getData().getPhone();
                    String profileImage = response.body().getData().getProfile_image();
                    String latitude = response.body().getData().getLatitude();
                    String longitude = response.body().getData().getLongitude();
                    String support_phone = response.body().getData().getSupport_phone();
                    String support_email = response.body().getData().getSupport_email();

                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);

                    Utilities.saveString(LoginActivity.this, "userName", userName + " " + lastName);
                    Utilities.saveInt(LoginActivity.this, "userID", userID);
                    Utilities.saveString(getApplicationContext(), "userEmail", userEmail);
                    Utilities.saveString(getApplicationContext(), "userPhone", userPhone);
                    Utilities.saveString(getApplicationContext(), "profileImage", profileImage);
                    Utilities.saveString(getApplicationContext(), "latitude", latitude);
                    Utilities.saveString(getApplicationContext(), "longitude", longitude);
                    Utilities.saveString(LoginActivity.this, "login_status", "yes");
                    Utilities.saveString(LoginActivity.this, "support_phone", support_email);
                    Utilities.saveString(LoginActivity.this, "support_email", support_phone);

                    startActivity(intent);
                    finish();

                }

             else if (status == 400){
                 Toast.makeText(getApplicationContext(), response.body().getMessage()  + "Failed", Toast.LENGTH_LONG).show();

              }
            }

            @Override
            public void onFailure(Call<UserResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();

            }
        });





    }

    public void SignUp(View v) {

        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));

    }

    //    @Override
//    public void onBackPressed() {
//        showCustomDialog();
//
//    }
    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(this);
        pDialog
                .setTitle("Exit")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.ic_baseline_exit_to_app_24)
                .setIconTint(R.color.color_Topbar_yellow)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.color_Topbar_yellow,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finish();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.black,
                        R.color.grey,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    public void terms(View view) {

        String url = "http://yuunri.com/terms_of_use";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}