package com.mtech.yunnriapp.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.bumptech.glide.Glide;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Room.Cart;
import com.mtech.yunnriapp.Room.MyDatabase;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.List;

public class AddCartActivity extends AppCompatActivity {
    RelativeLayout addtoCart;
    public static MyDatabase myDatabase;
    ImageView back_ic, btn_minus, btn_plus;
    TextView tv_Name_Cart, tv_title_cart, tv_Price_cart, tv_weight_cart, tvWeightUnit, tv_TotalCount, tv_price_total;
    ImageView imageView_cart;
    float amountTotal;
    int qty;
    float price;
    String productImage, productName, productWeight, prodcut_currency, prodcut_description, productPrice, amount;
    int productid, storeId;
    List<Cart> listCart;
    String counter = "1";
    ReadMoreTextView tvDescription;
int number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cart);

        tv_Name_Cart = findViewById(R.id.tv_name_cart);
        tv_title_cart = findViewById(R.id.tv_title_cart);
        tv_Price_cart = findViewById(R.id.tv_price_cart);
        tv_weight_cart = findViewById(R.id.tv_weight_cart);
        tvWeightUnit = findViewById(R.id.tvWeightUnit);
        imageView_cart = findViewById(R.id.cart_image);
        addtoCart = findViewById(R.id.addtoCart_layout);
        back_ic = findViewById(R.id.back_ic);
        btn_minus = findViewById(R.id.btn_minus_qty);
        tvDescription = findViewById(R.id.tvDescription);
        btn_plus = findViewById(R.id.btn_add_qty);
        tv_TotalCount = findViewById(R.id.tv_total_count);
        tv_price_total = findViewById(R.id.tv_total_price);
        storeId = Utilities.getInt(getApplicationContext(), "storeId");

        myDatabase = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "My_Cart").allowMainThreadQueries().build();


        //Utilities
        productImage = Utilities.getString(getApplicationContext(), "productImage");
        productName = Utilities.getString(getApplicationContext(), "productName");
        productWeight = Utilities.getString(getApplicationContext(), "productWeight");
        productPrice = Utilities.getString(getApplicationContext(), "productPrice");
        prodcut_description = Utilities.getString(getApplicationContext(), "prodcut_description");
        prodcut_currency = Utilities.getString(getApplicationContext(), "prodcut_currency");
        productid = Utilities.getInt(getApplicationContext(), "productId");


        Glide.with(getApplicationContext()).load(RetrofitClientInstance.BASE_URL_IMG + productImage).into(imageView_cart);
        tv_Name_Cart.setText(productName);
        tv_Price_cart.setText(productPrice + prodcut_currency);
        tv_price_total.setText(productPrice + prodcut_currency);
        if (!prodcut_description.equals("") && !prodcut_description.equals("")) {
            tvDescription.setText(prodcut_description);
        } else {
            tvDescription.setText("No Description");

        }

        tv_weight_cart.setText(productWeight);
        tvWeightUnit.setText(productWeight);


//        btn_plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                counter = counter + 1;
//                tv_TotalCount.setText(String.valueOf(counter));
//
//                qty = counter;
//                price = Float.parseFloat(productPrice);
//
//                amountTotal = price * Float.parseFloat(String.valueOf(tv_TotalCount.getText()));
//                amount = String.valueOf(amountTotal);
//                tv_price_total.setText(amount + prodcut_currency);
//            }
//        });
//
//        btn_minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (counter > 1) {
//                    counter = counter - 1;
//                    tv_TotalCount.setText(String.valueOf(counter));
//
//                    qty = counter;
//                    price = Float.parseFloat(productPrice);
//                    amountTotal = price * Float.parseFloat(String.valueOf(tv_TotalCount.getText()));;
//                    productPrice = String.valueOf(amountTotal);
//                    tv_price_total.setText(productPrice + prodcut_currency);
//
//
//                }
//
//            }
//        });

     btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                number = Integer.parseInt(tv_TotalCount.getText().toString());
                if (number <= 1) {
                    number = 1;
                } else {

                    number--;
                    tv_TotalCount.setText("" + number);
                }

                String price =productPrice;
                double value1 = price.isEmpty() ? 0.0 : Double.parseDouble(price);

                String quantity =tv_TotalCount.getText().toString();
                double value2 = quantity.isEmpty() ? 0.0 : Double.parseDouble(quantity);


                double a;
                a = value1 * value2;

                String finalValue = Double.toString(a);
                tv_price_total.setText(finalValue);

            }

        });

        btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                number = Integer.parseInt(tv_TotalCount.getText().toString());
                number++;

                tv_TotalCount.setText("" + number);
                counter = tv_TotalCount.getText().toString();

                String price = productPrice;
                double value1 = price.isEmpty() ? 0.0 : Double.parseDouble(price);

                String quantity =tv_TotalCount.getText().toString();
                double value2 = quantity.isEmpty() ? 0.0 : Double.parseDouble(quantity);


                double a;
                a = value1 * value2;

                String finalValue = Double.toString(a);
                tv_price_total.setText(finalValue);


            }
        });

        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        addtoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                boolean exists = myDatabase.cartDao().checkProduct(productid);

                if (exists){

                    update();

                }else {

                    add();

                }



            }

        });
    }

    private void update() {

        String quantityy = String.valueOf(myDatabase.cartDao().getquantity((productid)));
        myDatabase.cartDao().update(String.valueOf(Integer.parseInt(quantityy) + Integer.parseInt(tv_TotalCount.getText().toString())),
                productid);
        Intent intent = new Intent(getApplicationContext(), AddMoreActivity.class);
        startActivity(intent);
    }


    private void add() {

        if (!productName.equals("") && !productPrice.equals("")) {

            Cart cart = new Cart(productid, storeId, productName, String.valueOf(counter), productPrice, prodcut_description, productImage, prodcut_currency);

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    myDatabase.cartDao().insertAll(cart);
                }
            });
            Toast.makeText(AddCartActivity.this, "Product Added to Cart", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), AddMoreActivity.class);
            startActivity(intent);
            finish();
        }

    }
}