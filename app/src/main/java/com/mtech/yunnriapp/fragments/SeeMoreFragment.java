package com.mtech.yunnriapp.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.Server.ApiModelClass;
import com.mtech.yunnriapp.Server.Server;
import com.mtech.yunnriapp.Server.ServerCallback;
import com.mtech.yunnriapp.adapters.SeeMoreAdapter;
import com.mtech.yunnriapp.adapters.StoreAdapter;
import com.mtech.yunnriapp.models.CategoriesIDModel;
import com.mtech.yunnriapp.models.SeeMoreCategoryModel;
import com.mtech.yunnriapp.models.StoreModel;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class SeeMoreFragment extends Fragment implements SeeMoreAdapter.Callback {

    RecyclerView rvSubCategory;
    private RecyclerView freshFood_recyclerview;
    private ArrayList<CategoriesIDModel> categoriesIDModels;
    private ArrayList<SeeMoreCategoryModel> seeMoreCategoryModels;
    private ArrayList<StoreModel> listModels;
    SeeMoreAdapter adpter;
    int category_id, id;
    String cat_name, cat_image;
    KProgressHUD hud;
    ImageView imageView_category;
    TextView textView_cat_name, typeName, showMore;
    String subcategory_name, subcategory_image;
    int pos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_seemore, container, false);
        rvSubCategory = v.findViewById(R.id.rvSubCategory);
        //initializing
        freshFood_recyclerview = v.findViewById(R.id.freshfood_recyclerview);
        textView_cat_name = v.findViewById(R.id.cat_name);
        typeName = v.findViewById(R.id.typeName);
        imageView_category = v.findViewById(R.id.imageFood);
        showMore = v.findViewById(R.id.showMore);


        //Utilties
        category_id = Utilities.getInt(getContext(), "category_id");
        int userID = Utilities.getInt(getContext(), "userID");
        String input = Utilities.getString(getContext(), "profileImage");
        cat_name = Utilities.getString(getContext(), "cat_name");
        cat_image = Utilities.getString(getContext(), "cat_image");


        //set views
        textView_cat_name.setText(cat_name);
        Glide.with(getContext()).load(RetrofitClientInstance.BASE_URL_IMG + cat_image).centerCrop().placeholder(R.drawable.gradient).into(imageView_category);

        setTabTextApi();

        showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "See More", Toast.LENGTH_SHORT).show();
            }
        });

        return v;

    }


    public void setTabTextApi() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("category_id", String.valueOf(category_id));

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, Server.stores_by_category, getActivity(), params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    try {
                        seeMoreCategoryModels = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(result));

                        int status = object.getInt("status");
//                        String message = object.getString("Message");
                        if (status == 200) {
                            final JSONObject objUser = object.getJSONObject("data");
//
                            final JSONArray subcategories = objUser.getJSONArray("subcategories");
                            for (int j = 0; j < subcategories.length(); j++) {
//
                                JSONObject jsonObject1 = subcategories.getJSONObject(j);

                                String subcategory_name = jsonObject1.getString("subcategory_name");
                                int id = jsonObject1.getInt("id");
                                Utilities.saveString(requireActivity(), "categoryId", String.valueOf(id));
                                seeMoreCategoryModels.add(new SeeMoreCategoryModel(id, subcategory_name));
                                seeMoreCategoryModels.get(0).setSelected(true);
                                adpter = new SeeMoreAdapter(getContext(), seeMoreCategoryModels, SeeMoreFragment.this);
                                GridLayoutManager manager = new GridLayoutManager(getActivity(),2, RecyclerView.VERTICAL, false);
                                rvSubCategory.setHasFixedSize(true);
                                rvSubCategory.setLayoutManager(manager);
                                rvSubCategory.setAdapter(adpter);
                            }

//                                }


                        } else {
//                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Toast.makeText(getActivity(), ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    public void getSubCategoriesApis(final String categ_id, final String sub_id) {
        hud = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();
        Map<String, String> params = new HashMap<String, String>();
        params.put("category_id", categ_id);
        params.put("subcategory_id", sub_id);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");
        ApiModelClass.GetApiResponse(Request.Method.POST, Server.stores_by_subcategory, getActivity(), params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    try {
                        listModels = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(result));

                        int status = object.getInt("status");
                        String message = object.getString("message");
                        if (status == 200) {
                            hud.dismiss();
                            final JSONArray objUser = object.getJSONArray("data");

                            for (int i = 0; i < objUser.length(); i++) {
//
                                JSONObject jsonObject2 = objUser.getJSONObject(i);
                                String first_name = jsonObject2.getString("first_name");
                                String last_name = jsonObject2.getString("last_name");
                                String shop_name = jsonObject2.getString("shop_name");
                                String email = jsonObject2.getString("email");
                                String phone = jsonObject2.getString("phone");
                                String profile_image = jsonObject2.getString("profile_image");
                                String min_purchase = jsonObject2.getString("min_purchase");
                                String total_revenue = jsonObject2.getString("total_revenue");
                                String country = jsonObject2.getString("country");
                                String state = jsonObject2.getString("state");
                                String city = jsonObject2.getString("city");
                                String address = jsonObject2.getString("address");
//                                String delivery_charges = jsonObject2.getString("delivery_charges");
//                                String delivery_charges_currency = jsonObject2.getString("delivery_charges_currency");

                                String delivery_charges = "";
                                String delivery_charges_currency = "";
                                String zipcode = jsonObject2.getString("zipcode");
                                String latitude = jsonObject2.getString("latitude");
                                String longitude = jsonObject2.getString("longitude");
                                String self_description = jsonObject2.getString("self_description");
                                String delivery_area = jsonObject2.getString("delivery_area");
                                String bank_account_title = jsonObject2.getString("bank_account_title");
                                String bank_account_number = jsonObject2.getString("bank_account_number");
                                String bank_name = jsonObject2.getString("bank_name");
                                String branch_code = jsonObject2.getString("branch_code");
                                String vehicle_manufacturer = jsonObject2.getString("vehicle_manufacturer");
                                String vehicle_year = jsonObject2.getString("vehicle_year");
                                String vehicle_number = jsonObject2.getString("vehicle_number");
                                String shipping_area = jsonObject2.getString("shipping_area");
                                String min_purchase_currency = jsonObject2.getString("min_purchase_currency");
                                String shipping_area_latitude = jsonObject2.getString("shipping_area_latitude");
                                String shipping_area_longitude = jsonObject2.getString("shipping_area_longitude");
                                String business_license_number = "";
                                String business_registration_document = "";
//
//                                        String business_license_number = jsonObject2.getString("business_license_number");
//                                        String business_registration_document = jsonObject2.getString("business_registration_document");

                                String is_verified = jsonObject2.getString("is_verified");
                                String subcategory_name = jsonObject2.getString("subcategory_name");
                                String subcategory_image = jsonObject2.getString("subcategory_image");
                                int subcategory_id = jsonObject2.getInt("subcategory_id");


                                int id = jsonObject2.getInt("id");

                                listModels.add(new StoreModel(id, first_name, last_name, shop_name, email, phone, profile_image,
                                        min_purchase, total_revenue, country, state, city, address, zipcode, latitude, longitude,
                                        self_description, delivery_area, bank_account_title, bank_account_number, bank_name, delivery_charges, delivery_charges_currency,
                                        branch_code, vehicle_manufacturer, vehicle_year, vehicle_number, shipping_area,
                                        shipping_area_latitude, shipping_area_longitude, business_license_number,
                                        business_registration_document, is_verified,
                                        subcategory_id, subcategory_name, subcategory_image, min_purchase_currency));
                            }

                            freshFood_recyclerview.setVisibility(View.VISIBLE);
                            StoreAdapter adapter = new StoreAdapter(getContext(), listModels);
                            LinearLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 2);
                            freshFood_recyclerview.setLayoutManager(linearLayoutManager);
                            freshFood_recyclerview.setAdapter(adapter);


                        } else {
                            freshFood_recyclerview.setVisibility(View.GONE);
                            hud.dismiss();
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
//

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    hud.dismiss();
                    Toast.makeText(getActivity(), ERROR, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public void onItemClick(int pos, TextView tvproductName) {
        seeMoreCategoryModels.get(pos).setSelected(true);
//        tvproductName.setTextColor(getResources().getColor(R.color.color_Topbar_yellow));

        for (int i = 0; i < seeMoreCategoryModels.size(); i++) {
            if (i != pos) {
                seeMoreCategoryModels.get(pos).setSelected(false);
//                tvproductName.setTextColor(getResources().getColor(R.color.black));
            }
        }


        id = seeMoreCategoryModels.get(pos).getId();
        typeName.setText(seeMoreCategoryModels.get(pos).getName() + " Shops");
        getSubCategoriesApis(String.valueOf(category_id), String.valueOf(id));
    }
}