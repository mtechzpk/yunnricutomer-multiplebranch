package com.mtech.yunnriapp.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.adapters.CategoryAdapter;
import com.mtech.yunnriapp.models.CategoryModel;
import com.mtech.yunnriapp.models.CategoryResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CategoriesFragment extends Fragment {




    ProgressDialog progressDialog;
    private RecyclerView category_recyclerview;
    private CategoryAdapter adpter;
    private StaggeredGridLayoutManager manager;
    private List<CategoryModel> list;
    KProgressHUD hud;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_categories, container, false);


        category_recyclerview =v.findViewById(R.id.fragment_category_recyclerview);
        progressDialog = new ProgressDialog(getContext());
        hud = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<CategoryResponseModel> call = service.getAllCategories();
        call.enqueue(new Callback<CategoryResponseModel>() {
            @Override
            public void onResponse(Call<CategoryResponseModel> call, Response<CategoryResponseModel> response) {
                hud.dismiss();
                int status = response.body().getStatus();
                if (status == 200) if (response.body() != null) {
                    status = response.body().getStatus();
                }
                if (!String.valueOf(status).isEmpty() && status == 200) {
                    List dataList = response.body().getData();
                    if (dataList == null && getActivity() != null) {
                        Toast.makeText(getContext(), "No Record Found", Toast.LENGTH_LONG).show();
                    } else {
                        getCategories(category_recyclerview, dataList);

                    }
                } else if (status == 400) {
                    hud.dismiss();
                    Toast.makeText(getContext(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<CategoryResponseModel> call, Throwable t) {
                hud.dismiss();
                t.printStackTrace();

            }
        });

       return  v;
    }
    private void getCategories(RecyclerView category_recyclerview, List dataList) {
        adpter = new CategoryAdapter(getContext(),dataList);
        manager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        category_recyclerview.setHasFixedSize(true);
        category_recyclerview.setLayoutManager(manager);
        category_recyclerview.setAdapter(adpter);
    }
}
