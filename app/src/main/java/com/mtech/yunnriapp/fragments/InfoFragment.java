package com.mtech.yunnriapp.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.HomeActivity;
import com.mtech.yunnriapp.activities.LoginActivity;
import com.mtech.yunnriapp.models.UserResponseModel;
import com.mtech.yunnriapp.models.aboutInfo.AboutInfoResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InfoFragment extends Fragment implements OnMapReadyCallback {
    TextView tvCountry, tvState, tvAddress, tvZipCode, tvCity, tvDescription;
    KProgressHUD progressDialog;
    private GoogleMap locationmap;
    FusedLocationProviderClient fusedLocationProviderClient;
    static public Location currenlocation;
    private static final int REQUEST_CODE = 101;
    String store_lat = "", store_long = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        int storeId = Utilities.getInt(getActivity(), "storeId");
        store_lat = Utilities.getString(getActivity(), "store_lat");
        store_long = Utilities.getString(getActivity(), "store_long");
        tvCountry = view.findViewById(R.id.tvCountry);
        tvState = view.findViewById(R.id.tvState);
        tvDescription = view.findViewById(R.id.tvDescription);
        tvAddress = view.findViewById(R.id.tvAddress);
        tvZipCode = view.findViewById(R.id.tvZipCode);
        tvCity = view.findViewById(R.id.tvCity);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        fetchLastLocation();

        about_storeApi(String.valueOf(storeId));
        return view;
    }

    private void fetchLastLocation() {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);


            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null) {

                    currenlocation = location;
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.googlemap);
                    supportMapFragment.getMapAsync(InfoFragment.this);
                }

            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        locationmap = googleMap;
        if (!store_lat.equals("null") && !store_long.equals("null")) {
            LatLng latLng = new LatLng(Double.parseDouble(store_lat), Double.parseDouble(store_long));
            Toast.makeText(getContext(), latLng + "", Toast.LENGTH_SHORT).show();
            MarkerOptions markerOptions = new MarkerOptions().position(latLng);
            markerOptions.title("Store Location");
            locationmap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            locationmap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
            locationmap.addMarker(markerOptions);

        } else {
            LatLng latLng = new LatLng(currenlocation.getLatitude(), currenlocation.getLongitude());
            Toast.makeText(getContext(), latLng + "", Toast.LENGTH_SHORT).show();
            MarkerOptions markerOptions = new MarkerOptions().position(latLng);
            markerOptions.title("Current Location");
            locationmap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            locationmap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
            locationmap.addMarker(markerOptions);
        }


    }

    private void about_storeApi(String storeID) {
        progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<AboutInfoResponseModel> call = service.about_store(storeID);

        call.enqueue(new Callback<AboutInfoResponseModel>() {
            @Override
            public void onResponse(Call<AboutInfoResponseModel> call, Response<AboutInfoResponseModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    String country = response.body().getData().getAbout_store().getCountry();
                    String state = response.body().getData().getAbout_store().getState();
                    String city = response.body().getData().getAbout_store().getCity();
                    String address = response.body().getData().getAbout_store().getAddress();
                    String zipcode = response.body().getData().getAbout_store().getZipcode();
                    String descriptions = response.body().getData().getAbout_store().getSelf_description();
                    int total_reviews = response.body().getData().getTotal_reviews();
                    float overall_review_score = response.body().getData().getOverall_review_score();
                    if (!TextUtils.isEmpty(address)) {
                        tvAddress.setText(address);
                    }
                    if (!TextUtils.isEmpty(descriptions)) {
                        tvDescription.setText(descriptions);
                    }
                    if (!TextUtils.isEmpty(country)) {
                        tvCountry.setText(country);
                    }
                    if (!TextUtils.isEmpty(state)) {
                        tvState.setText(state);
                    }
                    if (!TextUtils.isEmpty(city)) {
                        tvCity.setText(city);
                    }

                } else if (status == 400) {
                    Toast.makeText(getActivity(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<AboutInfoResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();

            }
        });


    }

}