package com.mtech.yunnriapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.activities.WebViewActivity;

public class MoreOptionsFragment extends Fragment implements View.OnClickListener{
    View v;
    RelativeLayout rl_frequentlyaskedquestions,rl_languages,rl_termsconditions,rl_privacypolicy,
            rl_Deliveryreclamation,rl_Imprintnotice,aboutus,rlRate,rlshare;
    String url="",title;
    public MoreOptionsFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=inflater.inflate (R.layout.fragment_more_options, container, false);


        init(v);

        return v;
    }

    private void init(View v) {
        rl_frequentlyaskedquestions=v.findViewById (R.id.rl_frequentlyaskedquestions);
        rl_languages=v.findViewById (R.id.rl_languages);
        rl_termsconditions=v.findViewById (R.id.rl_termsconditions);
        rl_privacypolicy=v.findViewById (R.id.rl_privacypolicy);
        rl_Deliveryreclamation=v.findViewById (R.id.rl_Deliveryreclamation);
        rl_Imprintnotice=v.findViewById (R.id.rl_Imprintnotice);
        aboutus=v.findViewById (R.id.aboutus);
        rlRate=v.findViewById (R.id.rlRate);
        rlshare=v.findViewById (R.id.rlshare);

        rl_frequentlyaskedquestions.setOnClickListener (this);
        rl_languages.setOnClickListener (this);
        rl_termsconditions.setOnClickListener (this);
        rl_privacypolicy.setOnClickListener (this);
        rl_Deliveryreclamation.setOnClickListener (this);
        rl_Imprintnotice.setOnClickListener (this);
        rlRate.setOnClickListener(this);
        aboutus.setOnClickListener (this);
        rlshare.setOnClickListener (this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId ()){
            case R.id.rl_frequentlyaskedquestions:
                title="Frequently Asked Questions";
                url="http://yuunri.com/faq/";
                fragmenttoActivitywithData(url,title);
                break;
            case  R.id.rl_Deliveryreclamation:
                url="http://yuunri.com/delivery-and-reclamation-process/";
                title="Delivery and Reclamation";
                fragmenttoActivitywithData(url, title);
            break;
            case  R.id.aboutus:
                title="About us";
                url="http://yuunri.com/about-us/";
                fragmenttoActivitywithData(url, title);
                break;
            case  R.id.rl_termsconditions:
                url="http://yuunri.com/term-of-use/";
                title="Terms Conditions";
                fragmenttoActivitywithData(url, title);
                break;
            case  R.id.rl_privacypolicy:
                url="http://yuunri.com/privacy-policy/";
                title="Privacy Policy";
                fragmenttoActivitywithData(url, title);
                break;
            case  R.id.rl_Imprintnotice:
                title="Imprint/ Legal Notice";
                url="http://yuunri.com/imprint-legal-policy/";
                fragmenttoActivitywithData(url, title);
                break;
            case  R.id.rlRate:
                Uri uri = Uri.parse("https://play.google.com/store/apps"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case  R.id.rlshare:
                Uri uri1 = Uri.parse("https://play.google.com/store/apps"); // missing 'http://' will cause crashed
                Intent intent1 = new Intent(Intent.ACTION_VIEW, uri1);
                startActivity(intent1);
                break;
        }
    }

    private void fragmenttoActivitywithData(String url,String title) {
        Intent i = new Intent(getActivity(), WebViewActivity.class);
        i.putExtra("URL",url);
        i.putExtra("Title",title);
        startActivity(i);
        ((Activity) getActivity()).overridePendingTransition(0, 0);
    }
}