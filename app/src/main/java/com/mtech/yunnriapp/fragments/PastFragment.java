package com.mtech.yunnriapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.adapters.CurrentOrdersAdapter;
import com.mtech.yunnriapp.adapters.PastOrdersAdapter;
import com.mtech.yunnriapp.models.OrderDataModel;
import com.mtech.yunnriapp.models.OrderResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PastFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    String userId = "";
    TextView tvStatus;
    private LinearLayoutManager manager;
    private List<OrderDataModel> currentOrderlist;
    private CurrentOrdersAdapter currentOrdersAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_current, container, false);
        userId = String.valueOf(Utilities.getInt(getContext(), "userID"));
        recyclerView = view.findViewById(R.id.currentorders_rcyclerview);
        tvStatus = view.findViewById(R.id.tvStatus);
        getCurrentOrders();
        return view;
    }

    private void getCurrentOrders() {

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<OrderResponseModel> call = service.pastOrders(userId);
        call.enqueue(new Callback<OrderResponseModel>() {
            @Override
            public void onResponse(Call<OrderResponseModel> call, Response<OrderResponseModel> response) {

                int status = response.body().getStatus();
                currentOrderlist = response.body().getData();
                if (currentOrderlist != null && !currentOrderlist.isEmpty()) {
                    if (status == 200) {
                        showcurrentOrders(currentOrderlist, recyclerView);
                        tvStatus.setVisibility(View.GONE);

                    } else {
                        tvStatus.setVisibility(View.VISIBLE);

                        Toast.makeText(getContext(), "No Past Order found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    tvStatus.setVisibility(View.VISIBLE);

                    Toast.makeText(getContext(), "No Past Order found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OrderResponseModel> call, Throwable t) {
                Toast.makeText(getContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                tvStatus.setVisibility(View.VISIBLE);
                t.printStackTrace();
            }
        });

    }

    private void showcurrentOrders(List<OrderDataModel> list, RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        PastOrdersAdapter adapter = new PastOrdersAdapter(getContext(), list);
        recyclerView.setAdapter(adapter);
    }


}