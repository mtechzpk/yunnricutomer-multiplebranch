package com.mtech.yunnriapp.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.yunnriapp.R;
import com.mtech.yunnriapp.adapters.ReviewAdapter;
import com.mtech.yunnriapp.models.ReviewModel;
import com.mtech.yunnriapp.models.aboutInfo.AboutInfoResponseModel;
import com.mtech.yunnriapp.networks.GetDateService;
import com.mtech.yunnriapp.networks.RetrofitClientInstance;
import com.mtech.yunnriapp.utils.Utilities;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewFragment extends Fragment {
    private List<ReviewModel> reviewModelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ReviewAdapter adapter;
    TextView rate, outOF;
    KProgressHUD progressDialog;
    RatingBar rating;
    TextView tvStatus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        int storeId = Utilities.getInt(getActivity(), "storeId");

        recyclerView = view.findViewById(R.id.review_recyclerView);
        rate = view.findViewById(R.id.rate);
        rating = view.findViewById(R.id.rating);
        outOF = view.findViewById(R.id.outOF);
        tvStatus = view.findViewById(R.id.tvStatus);
        about_storeApi(String.valueOf(storeId));
        return view;
    }


    private void about_storeApi(String storeID) {
        progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.color_Black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<AboutInfoResponseModel> call = service.about_store(storeID);

        call.enqueue(new Callback<AboutInfoResponseModel>() {
            @Override
            public void onResponse(Call<AboutInfoResponseModel> call, Response<AboutInfoResponseModel> response) {
                progressDialog.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
                    List datalist = response.body().getData().getAllReviewDataModels();
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    String country = response.body().getData().getAbout_store().getCountry();
                    String state = response.body().getData().getAbout_store().getState();
                    String city = response.body().getData().getAbout_store().getCity();
                    String address = response.body().getData().getAbout_store().getAddress();
                    String zipcode = response.body().getData().getAbout_store().getZipcode();
                    int total_reviews = response.body().getData().getTotal_reviews();
                    float overall_review_score = response.body().getData().getOverall_review_score();
                    if (!TextUtils.isEmpty(String.valueOf(overall_review_score))) {
                        outOF.setText("Out OF " + String.valueOf(overall_review_score) + " Reviews");
                    }
                    rating.setProgress((int) overall_review_score);
                    tvStatus.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(String.valueOf(total_reviews))) {
                        rate.setText(String.valueOf(total_reviews));
                    }
                    if (datalist.size() != 0) {
                        setData(recyclerView, datalist);

                    } else {
                        Toast.makeText(getActivity(), "Data not found", Toast.LENGTH_SHORT).show();
                        tvStatus.setVisibility(View.VISIBLE);
                    }
                } else if (status == 400) {
                    tvStatus.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<AboutInfoResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                tvStatus.setVisibility(View.VISIBLE);
                t.printStackTrace();

            }
        });


    }

    private void setData(RecyclerView rvAvailableDate, List datalist) {

        adapter = new ReviewAdapter(datalist, getActivity());
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        rvAvailableDate.setHasFixedSize(true);
        rvAvailableDate.setLayoutManager(manager);
        rvAvailableDate.setAdapter(adapter);
    }

}