package com.mtech.yunnriapp;


import com.mtech.yunnriapp.Room.Cart;

public interface CartClickListner {

    void onPlusClick(Cart cartItem);
    void onMinusClick(Cart cartItem);
    void onDeleteClick(Cart cartItem);

}
