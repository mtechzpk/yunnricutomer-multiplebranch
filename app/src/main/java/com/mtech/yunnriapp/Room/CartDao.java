package com.mtech.yunnriapp.Room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CartDao {


    @Insert
    public void insertAll(Cart cart);

    @Query("SELECT * FROM MyCart")
    public List<Cart> getAllCartItem();

    @Query("SELECT EXISTS (SELECT 1 FROM MyCart WHERE id=:id)")
    public int isAddToCart(int id);

    @Query("select COUNT (*) from MyCart")
    int countCart();

    @Query("DELETE FROM MyCart WHERE id=:id")
    public void deleteItem(int id);

    @Query("DELETE FROM MyCart")
    void deleteAll();

    @Query("UPDATE MyCart SET quantity = :quantity ,price= :price  WHERE id LIKE :id ")
    int updateItem(int id,String quantity,String price);



    @Insert
    public void insertAllFav(FavStoreModel Fav);

    @Query("SELECT * FROM MyFav")
    public List<FavStoreModel>getAllFav();

    @Query("SELECT EXISTS (SELECT 1 FROM MyFav WHERE storeId=:id)")
    public int isAddtoFav(int id);

    @Query("select COUNT (*) from MyFav")
    int countFav();

    @Query("DELETE FROM MyFav WHERE storeId=:id")
    public void deleteFavItem(int id);

    @Query("DELETE FROM MyFav")
    void deleteFavAll();

    @Query("UPDATE MyCart SET quantity =:quantity WHERE id = :id")
    void update(String quantity, int id);


    @Query("SELECT EXISTS (SELECT 1 FROM MyCart WHERE id = :id)")
    public boolean checkProduct(int id);

    @Query("SELECT quantity from MyCart WHERE  id = :id")
    public String getquantity(int id);


}
