package com.mtech.yunnriapp.Room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "MyFav")
public class FavStoreModel {
    @PrimaryKey
    private int storeId;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "catName")
    public String catName;
    @ColumnInfo(name = "minPurchase")
    public String minPurchase;

    @ColumnInfo(name = "currency")
    public String currency;
    @ColumnInfo(name = "image")
    public String image;
    @ColumnInfo(name = "isFavourite")
    public String isFav;

    public FavStoreModel(int storeId, String name, String catName, String minPurchase, String currency, String image, String isFav) {
        this.storeId = storeId;
        this.name = name;
        this.catName = catName;
        this.minPurchase = minPurchase;
        this.currency = currency;
        this.image = image;
        this.isFav = isFav;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getMinPurchase() {
        return minPurchase;
    }

    public void setMinPurchase(String minPurchase) {
        this.minPurchase = minPurchase;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }
}