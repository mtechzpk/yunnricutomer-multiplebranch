package com.mtech.yunnriapp.Room;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "MyCart")
public class Cart {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "storeId")
    public int store_id;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "quantity")
    public String quantity;
    @ColumnInfo(name = "price")
    public String price;
    @ColumnInfo(name = "desc")
    public String desc;
    @ColumnInfo(name = "image")
    public String image;
    @ColumnInfo(name = "prodcut_currency")
    public String prodcut_currency;

    public Cart(int id, int store_id, String name, String quantity, String price, String desc, String image, String prodcut_currency) {
        this.id = id;
        this.store_id = store_id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.desc = desc;
        this.image = image;
        this.prodcut_currency = prodcut_currency;
    }

    public String getProdcut_currency() {
        return prodcut_currency;
    }

    public void setProdcut_currency(String prodcut_currency) {
        this.prodcut_currency = prodcut_currency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
