package com.mtech.yunnriapp.models.aboutInfo;

import com.google.gson.annotations.SerializedName;

public class AboutInfoDataModel {
    @SerializedName("id")
    private int id;
    @SerializedName("country")
    private String country;
    @SerializedName("state")
    private String state;
    @SerializedName("city")
    private String city;
    @SerializedName("address")
    private String address;
    @SerializedName("self_description")
    private String self_description;
    @SerializedName("delivery_area")
    private String delivery_area;


    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;

    @SerializedName("zipcode")
    private String zipcode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSelf_description() {
        return self_description;
    }

    public void setSelf_description(String self_description) {
        this.self_description = self_description;
    }

    public String getDelivery_area() {
        return delivery_area;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public void setDelivery_area(String delivery_area) {
        this.delivery_area = delivery_area;
    }
}