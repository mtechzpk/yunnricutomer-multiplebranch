package com.mtech.yunnriapp.models;

public class SubCategoriesIDModel {
    int id = 0;
String subCategoryName;
boolean isSelected;

    public SubCategoriesIDModel(int id, String subCategoryName) {
        this.id = id;
        this.subCategoryName = subCategoryName;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
