package com.mtech.yunnriapp.models;

public class NotificationModel {
int id;
    String notification_from,notification_to,notification,created_at;

    public NotificationModel(int id, String notification_from, String notification_to, String notification, String created_at) {
        this.id = id;
        this.notification_from = notification_from;
        this.notification_to = notification_to;
        this.notification = notification;
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotification_from() {
        return notification_from;
    }

    public void setNotification_from(String notification_from) {
        this.notification_from = notification_from;
    }

    public String getNotification_to() {
        return notification_to;
    }

    public void setNotification_to(String notification_to) {
        this.notification_to = notification_to;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
