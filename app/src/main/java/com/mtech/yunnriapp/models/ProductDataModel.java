package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

import java.util.List;

public class ProductDataModel {

    @SerializedName("id")
    private int id;
    @SerializedName("subcategory_name")
    private String product_category;
    @SerializedName("products")
    private List<ProductlistModel> products;

    public ProductDataModel(int id, String product_category, List<ProductlistModel> products) {
        this.id = id;
        this.product_category = product_category;
        this.products = products;
    }

    public ProductDataModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct_category() {
        return product_category;
    }

    public void setProduct_category(String product_category) {
        this.product_category = product_category;
    }

    public List<ProductlistModel> getProducts() {
        return products;
    }

    public void setProducts(List<ProductlistModel> products) {
        this.products = products;
    }
}
