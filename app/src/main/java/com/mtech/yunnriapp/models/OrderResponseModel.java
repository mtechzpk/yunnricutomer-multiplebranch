package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderResponseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<OrderDataModel> data;

    public OrderResponseModel(int status, String message, List<OrderDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OrderDataModel> getData() {
        return data;
    }

    public void setData(List<OrderDataModel> data) {
        this.data = data;
    }
}
