package com.mtech.yunnriapp.models;

public class ReviewModel {

    String name,date;
    int food,delivery;

    public ReviewModel(String name, String date, int food, int delivery) {
        this.name = name;
        this.date = date;
        this.food = food;
        this.delivery = delivery;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getFood() {
        return food;
    }

    public void setFood(int food) {
        this.food = food;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }
}
