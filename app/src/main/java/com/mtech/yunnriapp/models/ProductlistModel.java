package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

public class ProductlistModel {
    @SerializedName("id")
    private int id;
    @SerializedName("category_id")
    private int category_id;
    @SerializedName("subcategory_id")
    private int subcategory_id;
    @SerializedName("store_id")
    private int store_id;
    @SerializedName("product_category_id")
    private int product_category_id;
    @SerializedName("product_name")
    private String product_name;
    @SerializedName("image")
    private String image;
    @SerializedName("price")
    private String price;
    @SerializedName("unit_weight")
    private String unit_weight;
    @SerializedName("price_per_unit_weight")
    private String price_per_unit_weight;
    @SerializedName("discount_in_percentage")
    private String discount_in_percentage;
    @SerializedName("amount_after_discount")
    private String amount_after_discount;
    @SerializedName("status")
    private String status;
    @SerializedName("price_currency")
    private String price_currency;
    @SerializedName("product_description")
    private String product_description;

    public ProductlistModel(int id, int category_id, int subcategory_id, int store_id, int product_category_id, String product_name, String image, String price, String unit_weight, String price_per_unit_weight, String discount_in_percentage, String amount_after_discount, String status, String price_currency, String product_description) {
        this.id = id;
        this.category_id = category_id;
        this.subcategory_id = subcategory_id;
        this.store_id = store_id;
        this.product_category_id = product_category_id;
        this.product_name = product_name;
        this.image = image;
        this.price = price;
        this.unit_weight = unit_weight;
        this.price_per_unit_weight = price_per_unit_weight;
        this.discount_in_percentage = discount_in_percentage;
        this.amount_after_discount = amount_after_discount;
        this.status = status;
        this.price_currency = price_currency;
        this.product_description = product_description;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getPrice_currency() {
        return price_currency;
    }

    public void setPrice_currency(String price_currency) {
        this.price_currency = price_currency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(int subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public int getProduct_category_id() {
        return product_category_id;
    }

    public void setProduct_category_id(int product_category_id) {
        this.product_category_id = product_category_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit_weight() {
        return unit_weight;
    }

    public void setUnit_weight(String unit_weight) {
        this.unit_weight = unit_weight;
    }

    public String getPrice_per_unit_weight() {
        return price_per_unit_weight;
    }

    public void setPrice_per_unit_weight(String price_per_unit_weight) {
        this.price_per_unit_weight = price_per_unit_weight;
    }

    public String getDiscount_in_percentage() {
        return discount_in_percentage;
    }

    public void setDiscount_in_percentage(String discount_in_percentage) {
        this.discount_in_percentage = discount_in_percentage;
    }

    public String getAmount_after_discount() {
        return amount_after_discount;
    }

    public void setAmount_after_discount(String amount_after_discount) {
        this.amount_after_discount = amount_after_discount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}