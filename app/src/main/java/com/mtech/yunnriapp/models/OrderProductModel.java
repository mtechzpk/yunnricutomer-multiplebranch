package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

public class OrderProductModel {

    @SerializedName("id")
    private int id;
    @SerializedName("order_unique_id")
    private String order_unique_id;
    @SerializedName("store_id")
    private String store_id;
    @SerializedName("product_id")
    private String product_id;
    @SerializedName("quantity")
    private String quantity;
    @SerializedName("product_price")
    private String product_price;
    @SerializedName("product_details")
    private ProductDetailsModel product_details;

    public OrderProductModel(int id, String order_unique_id, String store_id, String product_id, String quantity, String product_price, ProductDetailsModel product_details) {
        this.id = id;
        this.order_unique_id = order_unique_id;
        this.store_id = store_id;
        this.product_id = product_id;
        this.quantity = quantity;
        this.product_price = product_price;
        this.product_details = product_details;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrder_unique_id() {
        return order_unique_id;
    }

    public void setOrder_unique_id(String order_unique_id) {
        this.order_unique_id = order_unique_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public ProductDetailsModel getProduct_details() {
        return product_details;
    }

    public void setProduct_details(ProductDetailsModel product_details) {
        this.product_details = product_details;
    }
}
