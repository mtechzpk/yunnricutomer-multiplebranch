package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;
import com.mtech.yunnriapp.models.CategoryDataModel;
import com.mtech.yunnriapp.models.StoreModel;

import java.util.List;

public class StoreResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<StoreModel> data;

    public StoreResponseModel(int status, String message, List<StoreModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StoreModel> getData() {
        return data;
    }

    public void setData(List<StoreModel> data) {
        this.data = data;
    }
}