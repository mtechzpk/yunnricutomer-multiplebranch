package com.mtech.yunnriapp.models;

public class CategoriesIDModel {
    int id = 0;

    public CategoriesIDModel(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
