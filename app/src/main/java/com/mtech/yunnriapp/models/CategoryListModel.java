package com.mtech.yunnriapp.models;

public class CategoryListModel {


    private String type;

    public CategoryListModel(String type) {
        this.type = type;
    }

    public CategoryListModel() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
