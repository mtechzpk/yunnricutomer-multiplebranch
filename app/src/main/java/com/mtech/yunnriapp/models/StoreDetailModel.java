package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

public class StoreDetailModel {

    @SerializedName("first_name")
    private String first_name;
    @SerializedName("last_name")
    private String last_name;
      @SerializedName("email")
    private String email;
      @SerializedName("phone")
    private String phone;
      @SerializedName("profile_image")
    private String profile_image;
      @SerializedName("delivery_time")
    private String delivery_time;
      @SerializedName("delivery_charges")
    private String delivery_charges;
      @SerializedName("min_purchase")
    private String min_purchase;

    public StoreDetailModel( String email, String phone, String profile_image, String delivery_time, String delivery_charges, String min_purchase) {
        this.email = email;
        this.phone = phone;
        this.profile_image = profile_image;
        this.delivery_time = delivery_time;
        this.delivery_charges = delivery_charges;
        this.min_purchase = min_purchase;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }

    public String getMin_purchase() {
        return min_purchase;
    }

    public void setMin_purchase(String min_purchase) {
        this.min_purchase = min_purchase;
    }
}
