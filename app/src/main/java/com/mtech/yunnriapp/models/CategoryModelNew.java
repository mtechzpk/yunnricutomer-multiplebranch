package com.mtech.yunnriapp.models;

public class CategoryModelNew {
    private String   name,email,phone,profile_image,delivery_charges,delivery_time,min_purchase,subcategory_name,subcategory_image;
int id;

    public CategoryModelNew(String name, String email, String phone, String profile_image, String delivery_charges, String delivery_time, String min_purchase,int id) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.profile_image = profile_image;
        this.delivery_charges = delivery_charges;
        this.delivery_time = delivery_time;
        this.min_purchase = min_purchase;
//        this.subcategory_name = subcategory_name;
//        this.subcategory_image = subcategory_image;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getMin_purchase() {
        return min_purchase;
    }

    public void setMin_purchase(String min_purchase) {
        this.min_purchase = min_purchase;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public String getSubcategory_name() {
//        return subcategory_name;
//    }
//
//    public String getSubcategory_image() {
//        return subcategory_image;
//    }
//
//    public void setSubcategory_image(String subcategory_image) {
//        this.subcategory_image = subcategory_image;
//    }
//
//    public void setSubcategory_name(String subcategory_name) {
//        this.subcategory_name = subcategory_name;
//    }
}
