package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<ProductDataModel> data;

    public ProductResponseModel(int status, String message, List<ProductDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductDataModel> getData() {
        return data;
    }

    public void setData(List<ProductDataModel> data) {
        this.data = data;
    }
}
