package com.mtech.yunnriapp.models.aboutInfo;

import com.google.gson.annotations.SerializedName;
import com.mtech.yunnriapp.models.UserDataModel;

public class AboutInfoResponseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataObjectModel data;


    public AboutInfoResponseModel(int status, String message, DataObjectModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataObjectModel getData() {
        return data;
    }

    public void setData(DataObjectModel data) {
        this.data = data;
    }
}
