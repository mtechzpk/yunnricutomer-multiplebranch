package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategoryModel {

    @SerializedName("id")
    private int id;
    @SerializedName("subcategory_name")
    private String subcategory_name;
    @SerializedName("subcategory_image")
    private String subcategory_image;

    public SubCategoryModel(int id, String subcategory_name, String subcategory_image) {
        this.id = id;
        this.subcategory_name = subcategory_name;
        this.subcategory_image = subcategory_image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubcategory_name() {
        return subcategory_name;
    }

    public void setSubcategory_name(String subcategory_name) {
        this.subcategory_name = subcategory_name;
    }

    public String getSubcategory_image() {
        return subcategory_image;
    }

    public void setSubcategory_image(String subcategory_image) {
        this.subcategory_image = subcategory_image;
    }
}