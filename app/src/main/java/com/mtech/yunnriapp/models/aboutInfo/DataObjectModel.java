package com.mtech.yunnriapp.models.aboutInfo;

import com.google.gson.annotations.SerializedName;
import com.mtech.yunnriapp.models.CategoryDataModel;

import java.util.List;

public class DataObjectModel {
    @SerializedName("total_reviews")
    private int total_reviews;
    @SerializedName("overall_review_score")
    private float overall_review_score;
    @SerializedName("about_store")
    private AboutInfoDataModel about_store;

    @SerializedName("all_reviews")
    private List<AllReviewDataModel> allReviewDataModels;

    public int getTotal_reviews() {
        return total_reviews;
    }

    public void setTotal_reviews(int total_reviews) {
        this.total_reviews = total_reviews;
    }

    public float getOverall_review_score() {
        return overall_review_score;
    }

    public void setOverall_review_score(float overall_review_score) {
        this.overall_review_score = overall_review_score;
    }

    public List<AllReviewDataModel> getAllReviewDataModels() {
        return allReviewDataModels;
    }

    public void setAllReviewDataModels(List<AllReviewDataModel> allReviewDataModels) {
        this.allReviewDataModels = allReviewDataModels;
    }

    public AboutInfoDataModel getAbout_store() {
        return about_store;
    }

    public void setAbout_store(AboutInfoDataModel about_store) {
        this.about_store = about_store;
    }
}