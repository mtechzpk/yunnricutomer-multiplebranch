package com.mtech.yunnriapp.models;

public class AddCartModel {
    private int id;
    private String quantity, name, price,desc;

    public AddCartModel(int id, String quantity, String name, String price,String desc) {
        this.id = id;
        this.quantity = quantity;
        this.name = name;
        this.price = price;
        this.desc = desc;

    }

    public AddCartModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
