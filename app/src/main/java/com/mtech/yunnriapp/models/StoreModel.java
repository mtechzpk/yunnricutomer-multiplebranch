package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

public class StoreModel {
    @SerializedName("id")
    private int id;
    @SerializedName("first_name")
    private String first_name;
    @SerializedName("last_name")
    private String last_name;
    @SerializedName("shop_name")
    private String shop_name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("profile_image")
    private String profile_image;
    @SerializedName("min_purchase")
    private String min_purchase;
    @SerializedName("total_revenue")
    private String total_revenue;
    @SerializedName("country")
    private String country;
    @SerializedName("state")
    private String state;
    @SerializedName("city")
    private String city;
    @SerializedName("address")
    private String address;
    @SerializedName("zipcode")
    private String zipcode;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("self_description")
    private String self_description;
    @SerializedName("delivery_area")
    private String delivery_area;
    @SerializedName("bank_account_title")
    private String bank_account_title;
    @SerializedName("bank_account_number")
    private String bank_account_number;
    @SerializedName("bank_name")
    private String bank_name;

    @SerializedName("delivery_charges")
    private String delivery_charges;
    @SerializedName("delivery_charges_currency")
    private String delivery_charges_currency;


    @SerializedName("branch_code")
    private String branch_code;
    @SerializedName("vehicle_manufacturer")
    private String vehicle_manufacturer;
    @SerializedName("vehicle_year")
    private String vehicle_year;
    @SerializedName("vehicle_number")
    private String vehicle_number;
    @SerializedName("shipping_area")
    private String shipping_area;
    @SerializedName("shipping_area_latitude")
    private String shipping_area_latitude;
    @SerializedName("shipping_area_longitude")
    private String shipping_area_longitude;
    @SerializedName("business_license_number")
    private String business_license_number;
    @SerializedName("business_registration_document")
    private String business_registration_document;
    @SerializedName("is_verified")
    private String is_verified;
    @SerializedName("subcategory_id")
    private int subcategory_id;
    @SerializedName("subcategory_name")
    private String subcategory_name;
    @SerializedName("subcategory_id")
    private String subcategory_image;
    @SerializedName("min_purchase_currency")
    private String min_purchase_currency;

    public StoreModel(int id, String first_name, String last_name, String shop_name, String email, String phone, String profile_image, String min_purchase, String total_revenue, String country, String state, String city, String address, String zipcode, String latitude, String longitude, String self_description, String delivery_area, String bank_account_title, String bank_account_number, String bank_name, String delivery_charges, String delivery_charges_currency, String branch_code, String vehicle_manufacturer, String vehicle_year, String vehicle_number, String shipping_area, String shipping_area_latitude, String shipping_area_longitude, String business_license_number, String business_registration_document, String is_verified, int subcategory_id, String subcategory_name, String subcategory_image, String min_purchase_currency) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.shop_name = shop_name;
        this.email = email;
        this.phone = phone;
        this.profile_image = profile_image;
        this.min_purchase = min_purchase;
        this.total_revenue = total_revenue;
        this.country = country;
        this.state = state;
        this.city = city;
        this.address = address;
        this.zipcode = zipcode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.self_description = self_description;
        this.delivery_area = delivery_area;
        this.bank_account_title = bank_account_title;
        this.bank_account_number = bank_account_number;
        this.bank_name = bank_name;
        this.delivery_charges = delivery_charges;
        this.delivery_charges_currency = delivery_charges_currency;
        this.branch_code = branch_code;
        this.vehicle_manufacturer = vehicle_manufacturer;
        this.vehicle_year = vehicle_year;
        this.vehicle_number = vehicle_number;
        this.shipping_area = shipping_area;
        this.shipping_area_latitude = shipping_area_latitude;
        this.shipping_area_longitude = shipping_area_longitude;
        this.business_license_number = business_license_number;
        this.business_registration_document = business_registration_document;
        this.is_verified = is_verified;
        this.subcategory_id = subcategory_id;
        this.subcategory_name = subcategory_name;
        this.subcategory_image = subcategory_image;
        this.min_purchase_currency = min_purchase_currency;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }

    public String getDelivery_charges_currency() {
        return delivery_charges_currency;
    }

    public void setDelivery_charges_currency(String delivery_charges_currency) {
        this.delivery_charges_currency = delivery_charges_currency;
    }

    public String getMin_purchase_currency() {
        return min_purchase_currency;
    }

    public void setMin_purchase_currency(String min_purchase_currency) {
        this.min_purchase_currency = min_purchase_currency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getMin_purchase() {
        return min_purchase;
    }

    public void setMin_purchase(String min_purchase) {
        this.min_purchase = min_purchase;
    }

    public String getTotal_revenue() {
        return total_revenue;
    }

    public void setTotal_revenue(String total_revenue) {
        this.total_revenue = total_revenue;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSelf_description() {
        return self_description;
    }

    public void setSelf_description(String self_description) {
        this.self_description = self_description;
    }

    public String getDelivery_area() {
        return delivery_area;
    }

    public void setDelivery_area(String delivery_area) {
        this.delivery_area = delivery_area;
    }

    public String getBank_account_title() {
        return bank_account_title;
    }

    public void setBank_account_title(String bank_account_title) {
        this.bank_account_title = bank_account_title;
    }

    public String getBank_account_number() {
        return bank_account_number;
    }

    public void setBank_account_number(String bank_account_number) {
        this.bank_account_number = bank_account_number;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBranch_code() {
        return branch_code;
    }

    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    public String getVehicle_manufacturer() {
        return vehicle_manufacturer;
    }

    public void setVehicle_manufacturer(String vehicle_manufacturer) {
        this.vehicle_manufacturer = vehicle_manufacturer;
    }

    public String getVehicle_year() {
        return vehicle_year;
    }

    public void setVehicle_year(String vehicle_year) {
        this.vehicle_year = vehicle_year;
    }

    public String getVehicle_number() {
        return vehicle_number;
    }

    public void setVehicle_number(String vehicle_number) {
        this.vehicle_number = vehicle_number;
    }

    public String getShipping_area() {
        return shipping_area;
    }

    public void setShipping_area(String shipping_area) {
        this.shipping_area = shipping_area;
    }

    public String getShipping_area_latitude() {
        return shipping_area_latitude;
    }

    public void setShipping_area_latitude(String shipping_area_latitude) {
        this.shipping_area_latitude = shipping_area_latitude;
    }

    public String getShipping_area_longitude() {
        return shipping_area_longitude;
    }

    public void setShipping_area_longitude(String shipping_area_longitude) {
        this.shipping_area_longitude = shipping_area_longitude;
    }

    public String getBusiness_license_number() {
        return business_license_number;
    }

    public void setBusiness_license_number(String business_license_number) {
        this.business_license_number = business_license_number;
    }

    public String getBusiness_registration_document() {
        return business_registration_document;
    }

    public void setBusiness_registration_document(String business_registration_document) {
        this.business_registration_document = business_registration_document;
    }

    public String getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(String is_verified) {
        this.is_verified = is_verified;
    }

    public int getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(int subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getSubcategory_name() {
        return subcategory_name;
    }

    public void setSubcategory_name(String subcategory_name) {
        this.subcategory_name = subcategory_name;
    }

    public String getSubcategory_image() {
        return subcategory_image;
    }

    public void setSubcategory_image(String subcategory_image) {
        this.subcategory_image = subcategory_image;
    }
}