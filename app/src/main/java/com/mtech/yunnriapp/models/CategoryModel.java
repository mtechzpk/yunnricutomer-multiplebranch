package com.mtech.yunnriapp.models;

public class CategoryModel {

    private String category;

    public CategoryModel(String category) {
        this.category = category;
    }

    public CategoryModel() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
