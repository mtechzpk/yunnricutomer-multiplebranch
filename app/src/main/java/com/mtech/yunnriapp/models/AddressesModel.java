package com.mtech.yunnriapp.models;

import android.graphics.drawable.Drawable;

import com.google.gson.annotations.SerializedName;

public class AddressesModel {

    @SerializedName("id")
    private int id;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("street_and_house_number")
    private String street;
     @SerializedName("floor")
    private String floor;
     @SerializedName("address_type")
    private String address_type;
     @SerializedName("latitude")
    private String latitude;
     @SerializedName("longitude")
    private String longitude;

    public AddressesModel(int id, int user_id, String street, String floor, String address_type, String latitude, String longitude) {
        this.id = id;
        this.user_id = user_id;
        this.street = street;
        this.floor = floor;
        this.address_type = address_type;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getAddress_type() {
        return address_type;
    }

    public void setAddress_type(String address_type) {
        this.address_type = address_type;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}