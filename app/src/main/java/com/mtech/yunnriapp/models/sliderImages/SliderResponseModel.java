package com.mtech.yunnriapp.models.sliderImages;

import com.google.gson.annotations.SerializedName;
import com.mtech.yunnriapp.models.CategoryDataModel;

import java.util.List;

public class SliderResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<SliderImagesDataModel> data;

    public SliderResponseModel(int status, String message, List<SliderImagesDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SliderImagesDataModel> getData() {
        return data;
    }

    public void setData(List<SliderImagesDataModel> data) {
        this.data = data;
    }
}
