package com.mtech.yunnriapp.models;

public class CurrentOrdersModel {

    private String storeName,price,weight,dateTime;

    public CurrentOrdersModel(String storeName, String price, String weight, String dateTime) {
        this.storeName = storeName;
        this.price = price;
        this.weight = weight;
        this.dateTime = dateTime;
    }

    public CurrentOrdersModel() {
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
