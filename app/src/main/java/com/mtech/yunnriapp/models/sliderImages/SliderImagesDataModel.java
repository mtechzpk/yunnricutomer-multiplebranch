package com.mtech.yunnriapp.models.sliderImages;

import com.google.gson.annotations.SerializedName;

public class SliderImagesDataModel {
    @SerializedName("slider_image")
    private String slider_image;

    public SliderImagesDataModel( String slider_image) {
        this.slider_image = slider_image;
    }
    public String getSlider_image() {
        return slider_image;
    }

    public void setSlider_image(String slider_image) {
        this.slider_image = slider_image;
    }
}