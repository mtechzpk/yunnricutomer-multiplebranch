package com.mtech.yunnriapp.models;

public class FavouriteStoresModel {

    private Integer img;
    private String storeName,title,time,price,minPrice;

    public FavouriteStoresModel(Integer img, String storeName, String title, String time, String price, String minPrice) {
        this.img = img;
        this.storeName = storeName;
        this.title = title;
        this.time = time;
        this.price = price;
        this.minPrice = minPrice;
    }

    public FavouriteStoresModel() {
    }

    public Integer getImg() {
        return img;
    }

    public void setImg(Integer img) {
        this.img = img;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }
}
