package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddressResponseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<AddressesModel> data;

    public AddressResponseModel() {
    }

    public AddressResponseModel(int status, String message, List<AddressesModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AddressesModel> getData() {
        return data;
    }

    public void setData(List<AddressesModel> data) {
        this.data = data;
    }
}
