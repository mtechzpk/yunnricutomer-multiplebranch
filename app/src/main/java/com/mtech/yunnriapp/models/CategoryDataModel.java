package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryDataModel {
    @SerializedName("id")
    private int id = 1;
    @SerializedName("category_name")
    private String category_name;
    @SerializedName("category_image")
    private String category_image;
    @SerializedName("subcategories")
    private String subcategories;

    public CategoryDataModel(int id, String category_name, String category_image, String subcategories) {
        this.id = id;
        this.category_name = category_name;
        this.category_image = category_image;
        this.subcategories = subcategories;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_image() {
        return category_image;
    }

    public void setCategory_image(String category_image) {
        this.category_image = category_image;
    }

    public String getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(String subcategories) {
        this.subcategories = subcategories;
    }
}