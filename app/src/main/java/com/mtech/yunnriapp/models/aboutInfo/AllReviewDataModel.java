package com.mtech.yunnriapp.models.aboutInfo;

import com.google.gson.annotations.SerializedName;

public class AllReviewDataModel {
    @SerializedName("id")
    private int id = 1;
    @SerializedName("reviewed_by_name")
    private String reviewed_by_name;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("delivery_rating")
    private String delivery_rating;
    @SerializedName("food_rating")
    private String food_rating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReviewed_by_name() {
        return reviewed_by_name;
    }

    public void setReviewed_by_name(String reviewed_by_name) {
        this.reviewed_by_name = reviewed_by_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDelivery_rating() {
        return delivery_rating;
    }

    public void setDelivery_rating(String delivery_rating) {
        this.delivery_rating = delivery_rating;
    }

    public String getFood_rating() {
        return food_rating;
    }

    public void setFood_rating(String food_rating) {
        this.food_rating = food_rating;
    }
}