package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDataModel {

    @SerializedName("id")
    private int orderId;
    @SerializedName("order_unique_id")
    private String order_unique_id;
     @SerializedName("store_id")
     private String store_id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("total_amount")
    private String total_amount;
    @SerializedName("order_status")
    private String order_status;
    @SerializedName("is_paid")
    private String is_paid;
    @SerializedName("delivery_address")
    private String delivery_address;
    @SerializedName("amount_currency")
    private String amount_currency;
    @SerializedName("postcode")
    private String postcode;
    @SerializedName("city")
    private String city;
    @SerializedName("floor")
    private String floor;
    @SerializedName("company_name")
    private String company_name;
    @SerializedName("optional_note")
    private String optional_note;
     @SerializedName("full_name")
    private String full_name;
     @SerializedName("email")
    private String email;
     @SerializedName("phone")
    private String phone;
     @SerializedName("payment_method")
    private String payment_method;
     @SerializedName("voucher_or_coupon")
    private String voucher_or_coupon;
     @SerializedName("delivery_charges")
    private String delivery_charges;
     @SerializedName("created_at")
    private String created_at;

     @SerializedName("store_details")
    private StoreDetailModel store_details;

     @SerializedName("order_products")
    private List<OrderProductModel> order_products;

    public OrderDataModel(int orderId, String order_unique_id, String store_id, String user_id, String total_amount, String order_status, String is_paid, String delivery_address, String postcode, String city, String floor, String company_name, String optional_note, String full_name, String email, String phone, String payment_method, String voucher_or_coupon, String delivery_charges, String created_at, StoreDetailModel store_details, List<OrderProductModel> order_products) {
        this.orderId = orderId;
        this.order_unique_id = order_unique_id;
        this.store_id = store_id;
        this.user_id = user_id;
        this.total_amount = total_amount;
        this.order_status = order_status;
        this.is_paid = is_paid;
        this.delivery_address = delivery_address;
        this.postcode = postcode;
        this.city = city;
        this.floor = floor;
        this.company_name = company_name;
        this.optional_note = optional_note;
        this.full_name = full_name;
        this.email = email;
        this.phone = phone;
        this.payment_method = payment_method;
        this.voucher_or_coupon = voucher_or_coupon;
        this.delivery_charges = delivery_charges;
        this.created_at = created_at;
        this.store_details = store_details;
        this.order_products = order_products;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrder_unique_id() {
        return order_unique_id;
    }

    public void setOrder_unique_id(String order_unique_id) {
        this.order_unique_id = order_unique_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getIs_paid() {
        return is_paid;
    }

    public void setIs_paid(String is_paid) {
        this.is_paid = is_paid;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getOptional_note() {
        return optional_note;
    }

    public void setOptional_note(String optional_note) {
        this.optional_note = optional_note;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAmount_currency() {
        return amount_currency;
    }

    public void setAmount_currency(String amount_currency) {
        this.amount_currency = amount_currency;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getVoucher_or_coupon() {
        return voucher_or_coupon;
    }

    public void setVoucher_or_coupon(String voucher_or_coupon) {
        this.voucher_or_coupon = voucher_or_coupon;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public StoreDetailModel getStore_details() {
        return store_details;
    }

    public void setStore_details(StoreDetailModel store_details) {
        this.store_details = store_details;
    }

    public List<OrderProductModel> getOrder_products() {
        return order_products;
    }

    public void setOrder_products(List<OrderProductModel> order_products) {
        this.order_products = order_products;
    }
}
