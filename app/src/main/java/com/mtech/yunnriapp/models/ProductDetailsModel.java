package com.mtech.yunnriapp.models;

import com.google.gson.annotations.SerializedName;

public class ProductDetailsModel {

    @SerializedName("id")
    private int id;
    @SerializedName("product_name")
    private String product_name;
     @SerializedName("image")
    private String image;
     @SerializedName("price")
    private String price;
     @SerializedName("unit_weight")
    private String unit_weight;

    public ProductDetailsModel(int id, String product_name, String image, String price, String unit_weight) {
        this.id = id;
        this.product_name = product_name;
        this.image = image;
        this.price = price;
        this.unit_weight = unit_weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit_weight() {
        return unit_weight;
    }

    public void setUnit_weight(String unit_weight) {
        this.unit_weight = unit_weight;
    }
}
