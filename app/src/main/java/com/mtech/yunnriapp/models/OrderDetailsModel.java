package com.mtech.yunnriapp.models;

public class OrderDetailsModel {

    private  Integer image;
    private String weight,title,price;

    public OrderDetailsModel(Integer image, String weight, String title, String price) {
        this.image = image;
        this.weight = weight;
        this.title = title;
        this.price = price;
    }

    public OrderDetailsModel() {
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
